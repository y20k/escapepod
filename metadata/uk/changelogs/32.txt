# v1.0.1 – Sag Alles Ab

**2021-03-13**

— Виправлення сумісності з F-Droid: видалено непотрібну пропрієтарну залежність (див. https://codeberg.org/y20k/escapepod/issues64)
