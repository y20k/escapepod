# v1.0.4 – Sag Alles Ab

**28.04.2021**

- виправлено помилку із збереженням позиції відтворення
- початкова підтримка MP4
- оновлені переклади
