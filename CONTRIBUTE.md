How to contribute to Escapepod
===============================

### Help with translations
Translations are managed on [Weblate](https://hosted.weblate.org/projects/escapepod/). You can help out by refining the existing translations, or by submitting new languages.

### Report a bug or suggest a new feature
Escapepod does **not** have a public issue tracker for feature requests and bug reports. The app is a one-person project. Maintaining an issue tracker is too time consuming.

### Submit your own solutions
Escapepod does **not** accept pull requests. The app is a one-person project. Managing pull requests is too time consuming.