/*
 * CollectionAdapter.kt
 * Implements the CollectionAdapter class
 * A CollectionAdapter is a custom adapter providing podcast card views for a RecyclerView
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.collection

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.escapepod.Keys
import org.y20k.escapepod.R
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.wrappers.EpisodeLatestView
import org.y20k.escapepod.database.wrappers.PodcastWithLatestEpisodeWrapper
import org.y20k.escapepod.helpers.CollectionHelper
import org.y20k.escapepod.helpers.FileHelper
import org.y20k.escapepod.helpers.ImageHelper
import org.y20k.escapepod.helpers.PreferencesHelper
import org.y20k.escapepod.ui.PodcastViewHolder


/*
 * CollectionAdapter class
 */
class CollectionAdapter(private val context: Context, private val collectionDatabase: CollectionDatabase, private val episodeListener: EpisodeListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /* Define log tag */
    private val TAG: String = CollectionAdapter::class.java.simpleName


    /* Main class variables */
    private lateinit var collectionViewModel: CollectionViewModel
    private val imageCache = androidx.collection.LruCache<String, Bitmap>(24)
    private lateinit var layoutManager: LinearLayoutManager
    private var collection: List<PodcastWithLatestEpisodeWrapper> = listOf()
    private var expandedPodcastFeedLocation: String = PreferencesHelper.loadPodcastListExpandedFeedLocation()
    private var expandedPodcastPosition: Int = -1
    private var tapAnywherePlaybackEnabled: Boolean = PreferencesHelper.loadTapAnyWherePlayback()
    private var podcastHolderDrawn: Boolean = false
    private var podcastCoverSize: Int = 0


    /* Overrides onAttachedToRecyclerView from RecyclerView.Adapter  */
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        // create view model and observe changes in podcast list view model
        collectionViewModel = ViewModelProvider(context as AppCompatActivity)[CollectionViewModel::class.java]
        observeCollectionViewModel(context as LifecycleOwner)
        // start listening for changes in shared preferences
        PreferencesHelper.registerPreferenceChangeListener(sharedPreferenceChangeListener)
    }


    /* Overrides onDetachedFromRecyclerView from RecyclerView.Adapter  */
    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        // stop listening for changes in shared preferences
        PreferencesHelper.unregisterPreferenceChangeListener(sharedPreferenceChangeListener)
    }


    /* Overrides onCreateViewHolder from RecyclerView.Adapter  */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when (viewType) {
            Keys.VIEW_TYPE_ADD_NEW -> {
                // get view, put view into holder and return
                val addNewView: View = LayoutInflater.from(parent.context).inflate(R.layout.card_add_new_podcast, parent, false)
                return AddNewViewHolder(addNewView)
            }
            else -> {
                // get view, initialize view, put view into holder and return
                val podcastView: View = LayoutInflater.from(parent.context).inflate(R.layout.card_podcast, parent, false)
                val podcastViewHolder: PodcastViewHolder = PodcastViewHolder(podcastView)
                podcastViewHolder.initialize(context)
                podcastViewHolder.currentEpisodeView.initialize(context, collectionDatabase, episodeListener)
                return podcastViewHolder
            }
        }
    }


    /* Overrides onBindViewHolder from RecyclerView.Adapter  */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {

            // CASE ADD NEW CARD
            is AddNewViewHolder -> {
                // AddNewViewHolder is set up in onCreateViewHolder
            }

            // CASE PODCAST CARD
            is PodcastViewHolder -> {
                // get podcast from position
                val podcast: PodcastWithLatestEpisodeWrapper = collection[position]
                // get reference to PodcastViewHolder
                val podcastViewHolder: PodcastViewHolder = holder as PodcastViewHolder
                // set up podcast
                podcastViewHolder.setPodcast(podcast)
                podcastViewHolder.setPodcastName()
                // measure actual cover size
                if (!podcastHolderDrawn) {
                    podcastHolderDrawn = true
                    podcastViewHolder.podcastImageView.viewTreeObserver.addOnGlobalLayoutListener{
                        podcastCoverSize = podcastViewHolder.podcastImageView.height
                    }
                }
                podcastViewHolder.podcastImageView.setImageBitmap(ImageHelper.getPodcastCover(context, podcast.data.smallCover, podcastCoverSize, ignoreScreenDensity = true))

                // Load image in background
                CoroutineScope(Dispatchers.IO).launch {
                    val cachedBitmap = imageCache[podcast.data.smallCover]
                    val bitmap = cachedBitmap ?: ImageHelper.getPodcastCover(context, podcast.data.smallCover, podcastCoverSize, ignoreScreenDensity = true)
                    if (cachedBitmap == null) {
                        imageCache.put(podcast.data.smallCover, bitmap)
                    }
                    withContext(Dispatchers.Main) {
                        holder.podcastImageView.setImageBitmap(bitmap)
                    }
                }

                podcastViewHolder.podcastImageView.setImageBitmap(ImageHelper.getPodcastCover(context, podcast.data.smallCover, podcastCoverSize, ignoreScreenDensity = true))
                // set up recent episode
                if (podcast.episode != null) {
                    podcastViewHolder.currentEpisodeView.episode = podcast.episode.data
                    podcastViewHolder.currentEpisodeView.tapAnywherePlaybackEnabled = tapAnywherePlaybackEnabled
                    podcastViewHolder.currentEpisodeView.setEpisodeTitle()
                    podcastViewHolder.currentEpisodeView.setEpisodeButtons()
                    podcastViewHolder.currentEpisodeView.setEpisodePlaybackProgress()
                }
            }
        }
    }


    /* Overrides onBindViewHolder from RecyclerView.Adapter  */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: List<Any>) {

        if (payloads.isEmpty()) {
            // call regular onBindViewHolder method
            onBindViewHolder(holder, position)

        } else if (holder is PodcastViewHolder) {

            for (data in payloads) {
                when (data as Int) {
                    Keys.HOLDER_UPDATE_COVER -> {
                        // todo implement
                    }
                    Keys.HOLDER_UPDATE_NAME -> {
                        // todo implement
                    }
                    Keys.HOLDER_UPDATE_PLAYBACK_STATE -> {
                        // todo implement
                    }
                    Keys.HOLDER_UPDATE_PLAYBACK_PROGRESS -> {
                        // todo implement
                    }
                    Keys.HOLDER_UPDATE_DOWNLOAD_STATE -> {
                        // todo implement
                    }
                }
            }
        }
    }


    /* Overrides getItemViewType from RecyclerView.Adapter  */
    override fun getItemViewType(position: Int): Int {
        when (isPositionFooter(position)) {
            true -> return Keys.VIEW_TYPE_ADD_NEW
            false -> return Keys.VIEW_TYPE_PODCAST
        }
    }


    /* Overrides getItemCount from RecyclerView.Adapter  */
    override fun getItemCount(): Int {
        // +1 ==> the add podcast card
        return collection.size + 1
    }


    /* Removes a podcast from collection */
    fun removePodcast(context: Context, position: Int) {
        // delete folders and assets
        CollectionHelper.deletePodcastFolders(context, collection[position].data)
        // remove podcast from collection
        collectionViewModel.removePodcast(collection[position].data.remotePodcastFeedLocation)
    }


//    /* Deletes an episode download from collection */
//    fun deleteEpisode(mediaId: String) {
//        Log.v(TAG, "Deleting episode: $mediaId")
//        // remove audio reference in database
//        listViewModel.deleteEpisodeAudio(mediaId)
//    }


    /* Marks an episode as played in collection */
    fun markEpisodePlayed(mediaId: String) {
        Log.v(TAG, "Marking as played episode: $mediaId")
        // mark episode als played and update collection
        collectionViewModel.markEpisodePlayed(mediaId)
    }


    /* Get podcast for given position */
    fun getPodcast(position: Int): PodcastWithLatestEpisodeWrapper {
        return collection[position]
    }


    /* Determines if position is last */
    private fun isPositionFooter(position: Int): Boolean {
        return position == collection.size
    }


    /* Updates the podcast list - redraws the views with changed content */
    private fun updateRecyclerView(newCollection: List<PodcastWithLatestEpisodeWrapper>) {
        if (collection.isEmpty() && newCollection.isNotEmpty()) {
            // data set has been initialized - redraw the whole list
            collection = newCollection
            notifyDataSetChanged()
        } else {
            // store old collection temporarily
            val oldCollection: List<PodcastWithLatestEpisodeWrapper> = collection.map { it.copy() }
            // set new collection
            collection = newCollection
            // calculate differences between current collection and old collection - and inform this adapter about the changes
            val diffResult = DiffUtil.calculateDiff(CollectionDiffCallback(oldCollection), true)
            diffResult.dispatchUpdatesTo(this@CollectionAdapter)
        }
    }


    /* Updates and saves state of expanded podcast card in list */
    private fun savePodcastListExpandedState(position: Int = -1, remotePodcastFeedLocation: String = String()) {
        expandedPodcastFeedLocation = remotePodcastFeedLocation
        expandedPodcastPosition = position
        PreferencesHelper.savePodcastListExpandedFeedLocation(expandedPodcastFeedLocation)
    }


    /* Observe view model of podcast list */
    private fun observeCollectionViewModel(owner: LifecycleOwner) {
        collectionViewModel.podcastListLiveData.observe(owner, Observer<List<PodcastWithLatestEpisodeWrapper>> { newCollection ->
            //Log.v(TAG, "Time to load episode list from database: ${System.currentTimeMillis() - collectionViewModel.initializedTimestamp} ms.")
            val sortedCollection: List<PodcastWithLatestEpisodeWrapper> = newCollection.sortedByDescending { podcast -> podcast.data.latestEpisodeDate }
            updateRecyclerView(sortedCollection)
        } )
    }


    /*
     * Defines the listener for changes in shared preferences
     */
    private val sharedPreferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        when (key) {
            Keys.PREF_TAP_ANYWHERE_PLAYBACK -> tapAnywherePlaybackEnabled = PreferencesHelper.loadTapAnyWherePlayback()
        }
    }
    /*
     * End of declaration
     */


    /*
     * Inner class: ViewHolder for the Add New podcast action
     */
    private inner class AddNewViewHolder (listItemAddNewLayout: View) : RecyclerView.ViewHolder(listItemAddNewLayout) {
        val addNewPodcastButtonView: MaterialButton = listItemAddNewLayout.findViewById(R.id.card_add_new_station)
        val settingsButtonView: MaterialButton = listItemAddNewLayout.findViewById(R.id.card_settings)

        init {
            addNewPodcastButtonView.setOnClickListener {
                // show the add podcast dialog
                episodeListener.onAddNewButtonTapped()
            }
            settingsButtonView.setOnClickListener {
                // navigate to the settings screen
                settingsButtonView.findNavController().navigate(R.id.settings_destination)
            }
        }


    }
    /*
     * End of inner class
     */


//    /*
//     * Inner class: DiffUtil.Callback that determines changes in data - improves list performance
//     */
//    private inner class CollectionDiffCallback(val oldCollection: List<PodcastWithRecentEpisodesWrapper>): DiffUtil.Callback() {
//
//        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//            val oldPodcast: PodcastWithRecentEpisodesWrapper = oldCollection[oldItemPosition]
//            val newPodcast: PodcastWithRecentEpisodesWrapper = collection[newItemPosition]
//            return oldPodcast.data.remotePodcastFeedLocation == newPodcast.data.remotePodcastFeedLocation
//        }
//
//        override fun getOldListSize(): Int {
//            return oldCollection.size
//        }
//
//        override fun getNewListSize(): Int {
//            return collection.size
//        }
//
//        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//            val oldPodcast: PodcastWithRecentEpisodesWrapper = oldCollection[oldItemPosition]
//            val newPodcast: PodcastWithRecentEpisodesWrapper = collection[newItemPosition]
//
//            // compare relevant contents of podcast
//            if (oldPodcast.data.name != newPodcast.data.name) return false
//            if (oldPodcast.data.website != newPodcast.data.website) return false
//            if (oldPodcast.data.remoteImageFileLocation != newPodcast.data.remoteImageFileLocation) return false
//            if (oldPodcast.data.remotePodcastFeedLocation == expandedPodcastFeedLocation) return false // todo test and remove again if necessary
//            if (FileHelper.getFileSize(oldPodcast.data.cover.toUri()) != FileHelper.getFileSize(newPodcast.data.cover.toUri())) return false
//            if (FileHelper.getFileSize(oldPodcast.data.smallCover.toUri()) != FileHelper.getFileSize(newPodcast.data.smallCover.toUri())) return false
//
//            // compare relevant contents of recent episode - check most likely changes fist
//            if (oldPodcast.episodes.isNotEmpty() && newPodcast.episodes.isNotEmpty()) {
//                val oldEpisode: EpisodeMostRecentView = oldPodcast.episodes[0]
//                val newEpisode: EpisodeMostRecentView = newPodcast.episodes[0]
//                // check most likely changes fist
//                if (oldEpisode.data.isPlaying != newEpisode.data.isPlaying) return false
//                if (oldEpisode.data.playbackPosition != newEpisode.data.playbackPosition) return false
//                if (oldEpisode.data.manuallyDownloaded != newEpisode.data.manuallyDownloaded) return false
//                if (oldEpisode.data.audio != newEpisode.data.audio) return false
//                // check the rest afterwards
//                if (oldEpisode.data.guid != newEpisode.data.guid) return false
//                if (oldEpisode.data.title != newEpisode.data.title) return false
//                //if (oldEpisode.chapters != newEpisode.chapters) return false
//                if (oldEpisode.data.publicationDate != newEpisode.data.publicationDate) return false
//                if (oldEpisode.data.duration != newEpisode.data.duration) return false
//                if (oldEpisode.data.remoteCoverFileLocation != newEpisode.data.remoteCoverFileLocation) return false
//                if (oldEpisode.data.remoteAudioFileLocation != newEpisode.data.remoteAudioFileLocation) return false
//                if (FileHelper.getFileSize(oldEpisode.data.cover.toUri()) != FileHelper.getFileSize(newEpisode.data.cover.toUri())) return false
//                if (FileHelper.getFileSize(oldEpisode.data.smallCover.toUri()) != FileHelper.getFileSize(newEpisode.data.smallCover.toUri())) return false
//            }
//
//            // none of the above -> contents are the same
//            return true
//        }
//    }
//    /*
//     * End of inner class
//     */


    /*
 * Inner class: DiffUtil.Callback that determines changes in data - improves list performance
 */
    private inner class CollectionDiffCallback(val oldCollection: List<PodcastWithLatestEpisodeWrapper>): DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldPodcast: PodcastWithLatestEpisodeWrapper = oldCollection[oldItemPosition]
            val newPodcast: PodcastWithLatestEpisodeWrapper = collection[newItemPosition]
            return oldPodcast.data.remotePodcastFeedLocation == newPodcast.data.remotePodcastFeedLocation
        }

        override fun getOldListSize(): Int {
            return oldCollection.size
        }

        override fun getNewListSize(): Int {
            return collection.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            
            var result: Boolean = true
            val oldPodcast: PodcastWithLatestEpisodeWrapper = oldCollection[oldItemPosition]
            val newPodcast: PodcastWithLatestEpisodeWrapper = collection[newItemPosition]

            // compare relevant contents of podcast
            if (oldPodcast.data.name != newPodcast.data.name) result = false
            if (oldPodcast.data.website != newPodcast.data.website) result = false
            if (oldPodcast.data.remoteImageFileLocation != newPodcast.data.remoteImageFileLocation) result = false
            if (oldPodcast.data.remotePodcastFeedLocation == expandedPodcastFeedLocation) result = false // todo test and remove again if necessary
            if (FileHelper.getFileSize(oldPodcast.data.cover.toUri()) != FileHelper.getFileSize(newPodcast.data.cover.toUri())) result = false
            if (FileHelper.getFileSize(oldPodcast.data.smallCover.toUri()) != FileHelper.getFileSize(newPodcast.data.smallCover.toUri())) result = false

            // compare relevant contents of recent episode - check most likely changes fist
            if (oldPodcast.episode != null && newPodcast.episode != null) {
                val oldEpisode: EpisodeLatestView = oldPodcast.episode
                val newEpisode: EpisodeLatestView = newPodcast.episode
                // check most likely changes fist
                if (oldEpisode.data.isPlaying != newEpisode.data.isPlaying) result = false
                if (oldEpisode.data.playbackPosition != newEpisode.data.playbackPosition) result = false
                if (oldEpisode.data.manuallyDownloaded != newEpisode.data.manuallyDownloaded) result = false
                if (oldEpisode.data.audio != newEpisode.data.audio) result = false
                // check the rest afterwards
                if (oldEpisode.data.guid != newEpisode.data.guid) result = false
                if (oldEpisode.data.title != newEpisode.data.title) result = false
                //if (oldEpisode.chapters != newEpisode.chapters) result = false
                if (oldEpisode.data.publicationDate != newEpisode.data.publicationDate) result = false
                if (oldEpisode.data.duration != newEpisode.data.duration) result = false
                if (oldEpisode.data.remoteCoverFileLocation != newEpisode.data.remoteCoverFileLocation) result = false
                if (oldEpisode.data.remoteAudioFileLocation != newEpisode.data.remoteAudioFileLocation) result = false
                if (FileHelper.getFileSize(oldEpisode.data.cover.toUri()) != FileHelper.getFileSize(newEpisode.data.cover.toUri())) result = false
                if (FileHelper.getFileSize(oldEpisode.data.smallCover.toUri()) != FileHelper.getFileSize(newEpisode.data.smallCover.toUri())) result = false
            }

            // none of the above -> contents are the same
            //Log.e(TAG, "RESULT: ${newPodcast.data.name} $result")
            return result
        }
    }
    /*
     * End of inner class
     */
    
}