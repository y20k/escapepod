/*
 * EpisodeListener.kt
 * Implements the EpisodeListener interface
 * An EpisodeListener is standardizes the interactions that an episode ui element offers
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.collection

import org.y20k.escapepod.database.objects.Episode


/*
 * EpisodeListener interface
 */
interface EpisodeListener {
    fun onPlayButtonTapped(selectedEpisode: Episode)
    fun onMarkListenedButtonTapped(selectedEpisode: Episode)
    fun onDownloadButtonTapped(selectedEpisode: Episode)
    fun onDownloadAllButtonTapped(episodeRemotePodcastFeedLocation: String)
    fun onDeleteButtonTapped(selectedEpisode: Episode)
    fun onAddNewButtonTapped()
}