/*
 * PodcastAdapter.kt
 * Implements the PodcastAdapter class
 * A PodcastAdapter is a custom adapter providing views for all episodes of a podcast for a RecyclerView
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */

package org.y20k.escapepod.collection

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.net.toUri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import org.y20k.escapepod.Keys
import org.y20k.escapepod.R
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.objects.Episode
import org.y20k.escapepod.helpers.FileHelper
import org.y20k.escapepod.helpers.PreferencesHelper
import org.y20k.escapepod.ui.EpisodeViewHolder


/*
 * PodcastAdapter class
 */
class EpisodeAdapter (private val activity: Activity, private val collectionDatabase: CollectionDatabase, private val episodeListener: EpisodeListener, private val remotePodcastFeedLocation: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    /* Define log tag */
    private val TAG: String = EpisodeAdapter::class.java.simpleName


    /* Main class variables */
    private lateinit var podcastViewModel: PodcastViewModel
    private var episodeList: List<Episode> = listOf()
    private var episodeListUnfiltered: List<Episode> = listOf()
    private var tapAnywherePlaybackEnabled: Boolean = PreferencesHelper.loadTapAnyWherePlayback()
    var episodeListDisplayFilter: Int = Keys.FILTER_SHOW_ALL


    /* Overrides onAttachedToRecyclerView from RecyclerView.Adapter  */
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        // create view model and observe changes in episode list view model
        podcastViewModel = PodcastViewModelFactory(activity.application, remotePodcastFeedLocation).create(PodcastViewModel::class.java)
        observeCollectionViewModel(activity as LifecycleOwner)
    }


    /* Overrides onCreateViewHolder from RecyclerView.Adapter  */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val episodeView = LayoutInflater.from(parent.context).inflate(R.layout.element_episode, parent, false)
        val episodeViewHolder: EpisodeViewHolder = EpisodeViewHolder(episodeView)
        episodeViewHolder.initialize(activity, collectionDatabase, episodeListener)
        return episodeViewHolder
    }


    /* Overrides onBindViewHolder from RecyclerView.Adapter  */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // get episode from position
        val episode = episodeList[position]
        // get reference to EpisodeViewHolder
        val episodeViewHolder: EpisodeViewHolder = holder as EpisodeViewHolder
        // set up episode
        episodeViewHolder.episode = episode
        episodeViewHolder.tapAnywherePlaybackEnabled = tapAnywherePlaybackEnabled
        episodeViewHolder.setEpisodeTitle()
        episodeViewHolder.setEpisodeButtons()
        episodeViewHolder.setEpisodePlaybackProgress()
    }


    /* Overrides getItemCount from RecyclerView.Adapter */
    override fun getItemCount(): Int {
        return episodeList.size
    }


    /* Overrides getFilter from Filterable */
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun convertResultToString(resultValue: Any?): CharSequence {
                return super.convertResultToString(resultValue)
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString: String = constraint?.toString() ?: String()
                when (charString) {
                    Keys.FILTER_SHOW_DOWNLOADED.toString() -> {
//                        filtered = true
                        episodeListDisplayFilter = Keys.FILTER_SHOW_DOWNLOADED
                        episodeList = filterDownloadedEpisodes(episodeListUnfiltered)
                    }
                    else -> {
//                        filtered = false
                        episodeListDisplayFilter = Keys.FILTER_SHOW_ALL
                        episodeList = episodeListUnfiltered
                    }
                }
                return FilterResults().apply { values = episodeList }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.values != null) {
                    episodeList = results.values as List<Episode>
                    notifyDataSetChanged() // todo implement diffutil
                }
            }

        }
    }


    /* Observe view model of podcast with details */
    private fun observeCollectionViewModel(owner: LifecycleOwner) {
        podcastViewModel.episodeListLiveData.observe(owner, Observer<List<Episode>> { newEpisodeList ->
            val sortedEpisodeList: List<Episode> = newEpisodeList.sortedByDescending { episode -> episode.publicationDate }
            updateRecyclerView(sortedEpisodeList)
        })
    }


    /* Filters episodes that have been downloaded */
    private fun filterDownloadedEpisodes(episodeList: List<Episode>): List<Episode> {
        val filteredList = ArrayList<Episode>()
        episodeList.filter { episode ->
            (episode.audio.isNotEmpty()) }.forEach {
            filteredList.add(it)
        }
        return filteredList
    }


    /* Updates the episode list - redraws the views with changed content */
    private fun updateRecyclerView(updatedEpisodeList: List<Episode>) {
        if (episodeListUnfiltered.isNotEmpty()) {
            val newEpisodeList: List<Episode>
            // store old episode list temporarily
            val oldEpisodeList: List<Episode> = episodeList.map { it.copy() }

            // set the unfiltered episode list
            episodeListUnfiltered = updatedEpisodeList

            // filter episode list, if necessary
            when (episodeListDisplayFilter) {
                Keys.FILTER_SHOW_DOWNLOADED -> newEpisodeList = filterDownloadedEpisodes(updatedEpisodeList)
                else -> newEpisodeList = updatedEpisodeList
            }

            // set the filtered episode list
            episodeList = newEpisodeList

            // calculate differences between current episode list and old episode list - and inform this adapter about the changes
            val diffResult = DiffUtil.calculateDiff(EpisodeListDiffCallback(newEpisodeList, oldEpisodeList), true)
            diffResult.dispatchUpdatesTo(this@EpisodeAdapter)
        } else {
            // fist run: data set has been initialized - filter episode list, if necessary
            when (episodeListDisplayFilter) {
                Keys.FILTER_SHOW_DOWNLOADED -> episodeList = filterDownloadedEpisodes(updatedEpisodeList)
                else -> episodeList = updatedEpisodeList
            }
            episodeListUnfiltered = updatedEpisodeList
            notifyDataSetChanged()
        }
    }


    /*
     * Inner class: DiffUtil.Callback that determines changes in data - improves list performance
     */
    private inner class EpisodeListDiffCallback(val newEpisodeList: List<Episode>, val oldEpisodeList: List<Episode>): DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldEpisode: Episode = oldEpisodeList[oldItemPosition]
            val newEpisode: Episode = newEpisodeList[newItemPosition]
            return oldEpisode.guid == newEpisode.guid
        }

        override fun getOldListSize(): Int {
            return oldEpisodeList.size
        }

        override fun getNewListSize(): Int {
            return newEpisodeList.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldEpisode: Episode = oldEpisodeList[oldItemPosition]
            val newEpisode: Episode = newEpisodeList[newItemPosition]

            // compare relevant contents of podcast - check most likely changes fist
            if (oldEpisode.isPlaying != newEpisode.isPlaying) return false
            if (oldEpisode.playbackPosition != newEpisode.playbackPosition) return false
            if (oldEpisode.manuallyDownloaded != newEpisode.manuallyDownloaded) return false
            if (oldEpisode.audio != newEpisode.audio) return false
            // check the rest afterwards
            if (oldEpisode.title != newEpisode.title) return false
            if (oldEpisode.publicationDate != newEpisode.publicationDate) return false
            if (oldEpisode.duration != newEpisode.duration) return false
            if (oldEpisode.remoteCoverFileLocation != newEpisode.remoteCoverFileLocation) return false
            if (oldEpisode.remoteAudioFileLocation != newEpisode.remoteAudioFileLocation) return false
            if (FileHelper.getFileSize(oldEpisode.cover.toUri()) != FileHelper.getFileSize(newEpisode.cover.toUri())) return false
            if (FileHelper.getFileSize(oldEpisode.smallCover.toUri()) != FileHelper.getFileSize(newEpisode.smallCover.toUri())) return false
            // none of the above -> contents are the same
            return true
        }
    }
    /*
     * End of inner class
     */
}
