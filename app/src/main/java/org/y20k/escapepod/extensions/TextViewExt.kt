/*
 * TextViewExt.kt
 * Implements TextViewExt extension methods
 * Useful extension methods for TextView
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.extensions

import android.text.Html
import android.widget.TextView


private val TAG: String = "TextViewExt"


/* Puts cleaned up HTML to a text view */
fun TextView.setCleanHtml(htmlSource: String) {
    text = Html.fromHtml(htmlSource.replace(Regex("<img.+?>"), ""), Html.FROM_HTML_MODE_COMPACT) /* regex removes placeholder images */
}