/*
 * BasePlayerService.kt
 * Implements the BasePlayerService abstract class
 * BasePlayerService is Escapepod's foreground service that plays podcast audio
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */

package org.y20k.escapepod

import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import android.media.audiofx.AudioEffect
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.media3.common.AudioAttributes
import androidx.media3.common.C
import androidx.media3.common.ForwardingPlayer
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.analytics.AnalyticsListener
import androidx.media3.session.CommandButton
import androidx.media3.session.DefaultMediaNotificationProvider
import androidx.media3.session.LibraryResult
import androidx.media3.session.MediaLibraryService
import androidx.media3.session.MediaSession
import androidx.media3.session.SessionCommand
import androidx.media3.session.SessionCommands
import androidx.media3.session.SessionResult
import com.google.common.collect.ImmutableList
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.SettableFuture
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.objects.Episode
import org.y20k.escapepod.helpers.CollectionHelper
import org.y20k.escapepod.helpers.PreferencesHelper

/*
 * BasePlayerService class
 */
@UnstableApi
abstract class BasePlayerService: MediaLibraryService() {


    /* Define log tag */
    private val TAG: String = BasePlayerService::class.java.simpleName


    /* Main class variables */
    abstract val player: Player
    private lateinit var mediaSession: MediaLibrarySession
    private lateinit var collectionDatabase: CollectionDatabase
    private lateinit var sleepTimer: CountDownTimer
    private val handler: Handler = Handler(Looper.getMainLooper())
    private val librarySessionCallback = CustomMediaLibrarySessionCallback()
    private var sleepTimerTimeRemaining: Long = 0L
    private var sleepTimerRunning: Boolean = false
    private var upNextEpisodeMediaId: String = String()
    private var currentEpisodeMediaId: String = String()


    /* Overrides onCreate from Service */
    override fun onCreate() {
        super.onCreate()
        // get instance of database
        collectionDatabase = CollectionDatabase.getInstance(application)
        // get media ids of the current episode and the episode in Up Next queue
        upNextEpisodeMediaId = PreferencesHelper.loadUpNextMediaId()
        currentEpisodeMediaId = PreferencesHelper.loadCurrentMediaId()
        // initialize player and session
        initializePlayer()
        initializeSession()
        // initializeMediaItems()
        val notificationProvider: DefaultMediaNotificationProvider = CustomNotificationProvider()
        notificationProvider.setSmallIcon(R.drawable.ic_notification_app_icon_white_24dp)
        setMediaNotificationProvider(notificationProvider)
    }


    /* Overrides onDestroy from Service */
    override fun onDestroy() {
        // player.removeAnalyticsListener(analyticsListener)
        handler.removeCallbacksAndMessages(null)
        player.removeListener(playerListener)
        player.release()
        mediaSession.release()
        super.onDestroy()
    }


    /* Overrides onGetSession from MediaSessionService */
    override fun onGetSession(controllerInfo: MediaSession.ControllerInfo): MediaLibrarySession {
        return mediaSession
    }


    /* Initialize the player - override in implementation */
    abstract fun initializePlayer()


    /* Initializes the MediaSession */
    private fun initializeSession() {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = TaskStackBuilder.create(this).run {
                addNextIntent(intent)
                getPendingIntent(0, PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)
        }

        mediaSession = MediaLibrarySession.Builder(this, player, librarySessionCallback).apply {
            setSessionActivity(pendingIntent)
        }.build()
    }


    /* Starts sleep timer / adds default duration to running sleeptimer */
    private fun startSleepTimer(continueWithPreviousTimerDuration: Boolean) {
        // stop running timer
        if (sleepTimerTimeRemaining > 0L && this::sleepTimer.isInitialized) {
            handler.removeCallbacks(resetSleepTimerStateRunnable)
            sleepTimer.cancel()
        }
        // initialize timer
        val duration: Long
        if (continueWithPreviousTimerDuration) {
            duration = sleepTimerTimeRemaining
        } else {
            duration = Keys.SLEEP_TIMER_DURATION + sleepTimerTimeRemaining
        }
        sleepTimer = object: CountDownTimer(duration, Keys.SLEEP_TIMER_INTERVAL) {
            override fun onFinish() {
                Log.v(TAG, "Sleep timer finished. Sweet dreams.")
                sleepTimerTimeRemaining = 0L
                sleepTimerRunning = false
                player.pause()
            }
            override fun onTick(millisUntilFinished: Long) {
                sleepTimerRunning = true
                sleepTimerTimeRemaining = millisUntilFinished
            }
        }
        // start timer
        sleepTimer.start()
        // store timer state
        PreferencesHelper.saveSleepTimerRunning(isRunning = true)
    }


    /* Cancels sleep timer */
    private fun cancelSleepTimer(delayedReset: Boolean) {
        if (this::sleepTimer.isInitialized && sleepTimerTimeRemaining > 0L) {
            if (delayedReset) {
                handler.postDelayed(resetSleepTimerStateRunnable, 2500L)
            } else {
                resetSleepTimerState()
            }
            sleepTimer.cancel()
        }
        // store timer state
        PreferencesHelper.saveSleepTimerRunning(isRunning = false)
    }


    /* Resets the state of the sleep timer */
    private fun resetSleepTimerState() {
        sleepTimerTimeRemaining = 0L
        sleepTimerRunning = false
    }


    /* Handles transition to Up Next episode */
    private fun handleTransitionToUpNext(mediaItem: MediaItem?): String {
        // store mediaId of previous episode, if available
        var previousEpisodeMediaId: String = String()
        if (player.hasPreviousMediaItem()) {
            previousEpisodeMediaId = player.getMediaItemAt(/* index = */ 0).mediaId
            player.removeMediaItem(/* index = */ 0)
            PreferencesHelper.saveUpNextMediaId()
        }
        // save mediaId for current episode
        PreferencesHelper.saveCurrentMediaId(player.currentMediaItem?.mediaId ?: String())
        // resume playback where it was previously stopped
        CoroutineScope(IO).launch {
            if (mediaItem != null) {
                val episode: Episode? = collectionDatabase.episodeDao().findByMediaId(mediaItem.mediaId)
                withContext(Main) {
                    if (episode != null) {
                        val position: Long = if (episode.isFinished()) 0L else episode.playbackPosition
                        player.seekTo(position)
                    }

                }
            }
        }
        return previousEpisodeMediaId
    }


    /*
     * Runnable: Periodically saves playback position
     */
    private val periodicProgressUpdateRunnable: Runnable = object : Runnable {
        override fun run() {
            val mediaId: String = player.currentMediaItem?.mediaId ?: String()
            val position: Long =  player.currentPosition
            val isPlaying: Boolean = player.isPlaying
            CoroutineScope(IO).launch {
                collectionDatabase.episodeDao().updatePlaybackPosition(mediaId, position, isPlaying)
            }
            // use the handler to start runnable again after specified delay
            handler.postDelayed(this, 15000)
        }
    }
    /*
     * End of declaration
     */


    /*
     * Runnable: Reset the sleep timer state
     */
    private val resetSleepTimerStateRunnable: Runnable = object : Runnable {
        override fun run() {
            resetSleepTimerState()
        }
    }
    /*
     * End of declaration
     */


    /*
     * Custom MediaSession Callback that handles player commands
     */
    private inner class CustomMediaLibrarySessionCallback: MediaLibrarySession.Callback {

        override fun onGetLibraryRoot(session: MediaLibrarySession, browser: MediaSession.ControllerInfo, params: LibraryParams?): ListenableFuture<LibraryResult<MediaItem>> {
            return Futures.immediateFuture(LibraryResult.ofItem(CollectionHelper.getRootItem(), params))
        }

        override fun onGetChildren(session: MediaLibrarySession, browser: MediaSession.ControllerInfo, parentId: String, page: Int, pageSize: Int, params: LibraryParams?): ListenableFuture<LibraryResult<ImmutableList<MediaItem>>> {
            val future = SettableFuture.create<LibraryResult<ImmutableList<MediaItem>>>()
            CoroutineScope(IO).launch {
                val children = CollectionHelper.getChildren(this@BasePlayerService, parentId, collectionDatabase)
                if (children.isNotEmpty()) {
                    future.set(LibraryResult.ofItemList(children, params))
                }
                future.set(LibraryResult.ofError(LibraryResult.RESULT_ERROR_BAD_VALUE))
            }
            return future
        }

        override fun onGetItem(session: MediaLibrarySession, browser: MediaSession.ControllerInfo, mediaId: String): ListenableFuture<LibraryResult<MediaItem>> {
            val future = SettableFuture.create<LibraryResult<MediaItem>>()
            CoroutineScope(IO).launch {
                val episode: Episode? = collectionDatabase.episodeDao().findByMediaId(mediaId)
                if (episode != null) {
                    val item: MediaItem = CollectionHelper.buildMediaItem(this@BasePlayerService, episode)
                    future.set(LibraryResult.ofItem(item, /* params= */ null))
                } else {
                    future.set(LibraryResult.ofError(LibraryResult.RESULT_ERROR_BAD_VALUE))
                }
            }
            return future
        }

        override fun onAddMediaItems(mediaSession: MediaSession, controller: MediaSession.ControllerInfo, mediaItems: MutableList<MediaItem>): ListenableFuture<List<MediaItem>> {
            val updatedMediaItems: List<MediaItem> = mediaItems.map { mediaItem ->
                mediaItem.buildUpon().apply {
                    setUri(mediaItem.requestMetadata.mediaUri)
                }.build()
            }
            return Futures.immediateFuture(updatedMediaItems)
        }

        override fun onConnect(session: MediaSession, controller: MediaSession.ControllerInfo): MediaSession.ConnectionResult {
            // add custom commands
            val connectionResult: MediaSession.ConnectionResult  = super.onConnect(session, controller)
            val builder: SessionCommands.Builder = connectionResult.availableSessionCommands.buildUpon()
            builder.add(SessionCommand(Keys.CMD_UP_NEXT_UPDATED, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_START_SLEEP_TIMER, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_CANCEL_SLEEP_TIMER, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_REQUEST_SLEEP_TIMER_RUNNING, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_REQUEST_SLEEP_TIMER_REMAINING, Bundle.EMPTY))
            return MediaSession.ConnectionResult.accept(builder.build(), connectionResult.availablePlayerCommands);
        }

        override fun onSubscribe(session: MediaLibrarySession, browser: MediaSession.ControllerInfo,  parentId: String, params: LibraryParams?): ListenableFuture<LibraryResult<Void>> {
            val future = SettableFuture.create<LibraryResult<Void>>()
            CoroutineScope(IO).launch {
                val itemCount: Int = collectionDatabase.episodeDao().getSize()
                session.notifyChildrenChanged(browser, parentId, itemCount, params)
                future.set(LibraryResult.ofVoid())
            }
            return future
        }

        override fun onPlaybackResumption(mediaSession: MediaSession, controller: MediaSession.ControllerInfo ): ListenableFuture<MediaSession.MediaItemsWithStartPosition> {
            // note onPlaybackResumption is only called if player has no media items yet
            val future = SettableFuture.create<MediaSession.MediaItemsWithStartPosition>()
            CoroutineScope(IO).launch {
                // create list if media items for playback resumption
                val mediaItems: MutableList<MediaItem> = mutableListOf()
                val currentEpisode: Episode? = collectionDatabase.episodeDao().findByMediaId(currentEpisodeMediaId)
                if (currentEpisode != null) {
                    mediaItems.add(CollectionHelper.buildMediaItem(this@BasePlayerService, currentEpisode))
                }
                val result: MediaSession.MediaItemsWithStartPosition = if (mediaItems.isNotEmpty()) {
                    MediaSession.MediaItemsWithStartPosition(mediaItems, 0, currentEpisode?.playbackPosition ?: C.TIME_UNSET)
                } else {
                    MediaSession.MediaItemsWithStartPosition(emptyList(), C.INDEX_UNSET, C.TIME_UNSET)
                }
                future.set(result)
            }
            return future
        }

        override fun onCustomCommand(session: MediaSession, controller: MediaSession.ControllerInfo, customCommand: SessionCommand, args: Bundle): ListenableFuture<SessionResult> {
            when (customCommand.customAction) {
                Keys.CMD_UP_NEXT_UPDATED -> {
                    if (args.containsKey(Keys.EXTRA_UP_NEXT_MEDIA_ID)) {
                        upNextEpisodeMediaId = args.getString(Keys.EXTRA_UP_NEXT_MEDIA_ID) ?: String()
                    }
                }

//                Keys.CMD_ADD_TO_UP_NEXT -> {
//                    if (args.containsKey(Keys.EXTRA_UP_NEXT_MEDIA_ID)) {
//                        addToUpNext(args.getString(Keys.EXTRA_UP_NEXT_MEDIA_ID) ?: String())
//                    }
//                }
//                Keys.CMD_CLEAR_UP_NEXT -> {
//                    clearUpNext()
//                }

                Keys.CMD_START_SLEEP_TIMER -> {
                    startSleepTimer(continueWithPreviousTimerDuration = false)
                }
                Keys.CMD_CANCEL_SLEEP_TIMER -> {
                    cancelSleepTimer(delayedReset = false)
                }
                Keys.CMD_REQUEST_SLEEP_TIMER_RUNNING -> {
                    val resultBundle = Bundle()
                    resultBundle.putBoolean(Keys.EXTRA_SLEEP_TIMER_RUNNING, sleepTimerRunning)
                    return Futures.immediateFuture(SessionResult(SessionResult.RESULT_SUCCESS, resultBundle))
                }
                Keys.CMD_REQUEST_SLEEP_TIMER_REMAINING -> {
                    val resultBundle = Bundle()
                    resultBundle.putLong(Keys.EXTRA_SLEEP_TIMER_REMAINING, sleepTimerTimeRemaining)
                    return Futures.immediateFuture(SessionResult(SessionResult.RESULT_SUCCESS, resultBundle))
                }
            }
            return super.onCustomCommand(session, controller, customCommand, args)
        }

        override fun onPlayerCommandRequest(session: MediaSession, controller: MediaSession.ControllerInfo, playerCommand: Int): Int {
            // playerCommand = one of COMMAND_PLAY_PAUSE, COMMAND_PREPARE, COMMAND_STOP, COMMAND_SEEK_TO_DEFAULT_POSITION, COMMAND_SEEK_IN_CURRENT_MEDIA_ITEM, COMMAND_SEEK_TO_PREVIOUS_MEDIA_ITEM, COMMAND_SEEK_TO_PREVIOUS, COMMAND_SEEK_TO_NEXT_MEDIA_ITEM, COMMAND_SEEK_TO_NEXT, COMMAND_SEEK_TO_MEDIA_ITEM, COMMAND_SEEK_BACK, COMMAND_SEEK_FORWARD, COMMAND_SET_SPEED_AND_PITCH, COMMAND_SET_SHUFFLE_MODE, COMMAND_SET_REPEAT_MODE, COMMAND_GET_CURRENT_MEDIA_ITEM, COMMAND_GET_TIMELINE, COMMAND_GET_MEDIA_ITEMS_METADATA, COMMAND_SET_MEDIA_ITEMS_METADATA, COMMAND_CHANGE_MEDIA_ITEMS, COMMAND_GET_AUDIO_ATTRIBUTES, COMMAND_GET_VOLUME, COMMAND_GET_DEVICE_VOLUME, COMMAND_SET_VOLUME, COMMAND_SET_DEVICE_VOLUME, COMMAND_ADJUST_DEVICE_VOLUME, COMMAND_SET_VIDEO_SURFACE, COMMAND_GET_TEXT, COMMAND_SET_TRACK_SELECTION_PARAMETERS or COMMAND_GET_TRACK_INFOS. */
            // emulate headphone buttons
            // start/pause: adb shell input keyevent 85
            // next: adb shell input keyevent 87
            // prev: adb shell input keyevent 88
            when (playerCommand) {
                Player.COMMAND_SEEK_TO_NEXT_MEDIA_ITEM -> {
                    val currentEpisodeMediaId: String? = player.currentMediaItem?.mediaId
                    val currentPosition: Long = player.currentPosition
                    if (currentEpisodeMediaId != null) {
                        CoroutineScope(IO).launch {
                            collectionDatabase.episodeDao().updatePlaybackPosition(currentEpisodeMediaId, currentPosition, false)
                        }
                    }
                    return super.onPlayerCommandRequest(session, controller, playerCommand)
                }
                Player.COMMAND_SEEK_TO_NEXT ->  {
                    // override "skip to next" command - just skip forward instead
                    player.seekForward()
                    return SessionResult.RESULT_INFO_SKIPPED
                }
                Player.COMMAND_SEEK_TO_PREVIOUS ->  {
                    // override "skip to previous" command - just skip back instead
                    player.seekBack()
                    return SessionResult.RESULT_INFO_SKIPPED
                }
                Player.COMMAND_STOP -> {
                    // todo hide notification
                    player.pause()
//                    mediaSession.player.release()
//                    mediaSession.release()
//                    stopSelf()
//                    player.clearMediaItems()
//                    stopForeground(true)
                    return SessionResult.RESULT_INFO_SKIPPED
                }
                Player.COMMAND_PLAY_PAUSE -> {
                    return super.onPlayerCommandRequest(session, controller, playerCommand)
                }
                else -> {
                    return super.onPlayerCommandRequest(session, controller, playerCommand)
                }
            }
        }
    }
    /*
     * End of inner class
     */


    /*
     * NotificationProvider to customize Notification actions
     */
    private inner class CustomNotificationProvider: DefaultMediaNotificationProvider(this@BasePlayerService) {
        override fun getMediaButtons(session: MediaSession, playerCommands: Player.Commands, customLayout: ImmutableList<CommandButton>, showPauseButton: Boolean): ImmutableList<CommandButton> {
            val seekBack10sCommandButton = CommandButton.Builder()
                .setPlayerCommand(Player.COMMAND_SEEK_BACK)
                .setIconResId(R.drawable.ic_notification_skip_back_36dp)
                .setEnabled(true)
                .build()
            val playCommandButton = CommandButton.Builder()
                .setPlayerCommand(Player.COMMAND_PLAY_PAUSE)
                .setIconResId(if (mediaSession.player.isPlaying) R.drawable.ic_notification_pause_36dp else R.drawable.ic_notification_play_36dp)
                .setEnabled(true)
                .build()
            val seekForward30sCommandButton = CommandButton.Builder()
                .setPlayerCommand(Player.COMMAND_SEEK_FORWARD)
                .setIconResId(R.drawable.ic_notification_skip_forward_36dp)
                .setEnabled(true)
                .build()
            val commandButtons: MutableList<CommandButton> = mutableListOf(
                seekBack10sCommandButton,
                playCommandButton,
                seekForward30sCommandButton
            )
            return ImmutableList.copyOf(commandButtons)
        }
    }
    /*
     * End of inner class
     */


    /*
     * Custom Player for local playback
     */
    val localPlayer: Player by lazy {
        // step 1: create the local player
        val exoPlayer: ExoPlayer = ExoPlayer.Builder(this).apply {
            setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true)
            setHandleAudioBecomingNoisy(true)
            setSeekBackIncrementMs(Keys.SKIP_BACK_TIME_SPAN)
            setSeekForwardIncrementMs(Keys.SKIP_FORWARD_TIME_SPAN)
        }.build()
        exoPlayer.setPlaybackSpeed(PreferencesHelper.loadPlayerPlaybackSpeed())
        exoPlayer.addListener(playerListener)
        //exoPlayer.addAnalyticsListener(analyticsListener)
        // manually add seek to next, seek to previous, etc.
        val player = object : ForwardingPlayer(exoPlayer) {
            override fun getAvailableCommands(): Player.Commands {
                return super.getAvailableCommands().buildUpon()
                    .add(Player.COMMAND_SEEK_FORWARD)
                    .add(Player.COMMAND_SEEK_TO_NEXT)
                    .add(Player.COMMAND_SEEK_BACK)
                    .add(Player.COMMAND_SEEK_TO_PREVIOUS)
                    .add(Player.COMMAND_GET_CURRENT_MEDIA_ITEM)
                    .add(Player.COMMAND_SET_MEDIA_ITEM)
                    .build()
            }
            override fun isCommandAvailable(command: Int): Boolean {
                return availableCommands.contains(command)
            }
        }
        player
    }
    /*
     * End of declaration
     */


    /*
     * Player.Listener: Called when one or more player states changed.
     */
    val playerListener: Player.Listener = object : Player.Listener {

        //        // log number of episodes in player
//        override fun onTimelineChanged(timeline: Timeline, reason: Int) {
//            super.onTimelineChanged(timeline, reason)
//            Log.v(TAG, "Number of Episodes in player: ${player.mediaItemCount}")
//        }

        override fun onMediaItemTransition(mediaItem: MediaItem?, reason: Int) {
            super.onMediaItemTransition(mediaItem, reason)

            // check if Up Next episode started
            when (reason) {
                Player.MEDIA_ITEM_TRANSITION_REASON_AUTO -> {
                    val previousEpisodeMediaId: String = handleTransitionToUpNext(mediaItem)
                    // mark previously played episode as played
                    CoroutineScope(IO).launch {
                        collectionDatabase.episodeDao().markPlayed(previousEpisodeMediaId)
                    }
                }
                Player.MEDIA_ITEM_TRANSITION_REASON_SEEK -> {
                    handleTransitionToUpNext(mediaItem)
                    // playback position of previous episode is stored in onPlayerCommandRequest (COMMAND_SEEK_TO_NEXT_MEDIA_ITEM)
                }
                else -> {
                    // do nothing
                }
            }
//            // Log transition reasons
//            when (reason) {
//                // media item has been repeated
//                Player.MEDIA_ITEM_TRANSITION_REASON_REPEAT -> Log.d(TAG, "0 MEDIA_ITEM_TRANSITION_REASON_REPEAT - count: ${player.mediaItemCount} // item: ${mediaItem?.mediaMetadata?.title} ") // todo remove
//                // playback automatically transitioned to next media item (previous episode finished and Up Next episode started automatically)
//                Player.MEDIA_ITEM_TRANSITION_REASON_AUTO -> Log.d(TAG, "1 MEDIA_ITEM_TRANSITION_REASON_AUTO - count: ${player.mediaItemCount} // item: ${mediaItem?.mediaMetadata?.title} ") // todo remove
//                // seek to another media item has occurred
//                Player.MEDIA_ITEM_TRANSITION_REASON_SEEK -> Log.d(TAG, "2 MEDIA_ITEM_TRANSITION_REASON_SEEK - count: ${player.mediaItemCount} // item: ${mediaItem?.mediaMetadata?.title} ") // todo remove
//                // current media item has changed because of a change in the playlist - a new media item was started
//                Player.MEDIA_ITEM_TRANSITION_REASON_PLAYLIST_CHANGED -> Log.d(TAG, "3 MEDIA_ITEM_TRANSITION_REASON_PLAYLIST_CHANGED - count: ${player.mediaItemCount} // item: ${mediaItem?.mediaMetadata?.title} ") // todo remove
//            }
        }

        override fun onIsPlayingChanged(isPlaying: Boolean) {
            super.onIsPlayingChanged(isPlaying)
            val mediaId: String = player.currentMediaItem?.mediaId ?: String()
            val currentPosition: Long = player.currentPosition
            // store state of playback
            PreferencesHelper.saveIsPlaying(isPlaying)
            PreferencesHelper.saveCurrentMediaId(mediaId)
            if (mediaId == upNextEpisodeMediaId) PreferencesHelper.saveUpNextMediaId()
            // save playback state of episode to database
            CoroutineScope(IO).launch {
                collectionDatabase.episodeDao().updatePlaybackPosition(mediaId, currentPosition, isPlaying)
            }

            if (isPlaying) {
                // playback is active - start to periodically save the playback position to database
                handler.removeCallbacks(periodicProgressUpdateRunnable)
                handler.postDelayed(periodicProgressUpdateRunnable, 0)
                // restart the sleep time if there is still time on the clock
                if (sleepTimerTimeRemaining > 0) startSleepTimer(continueWithPreviousTimerDuration = true)
                // add the Up Next episode to the queue - used when playback was started by the system not the user
                if (player.mediaItemCount == 1 && player.currentMediaItem?.mediaId != upNextEpisodeMediaId && upNextEpisodeMediaId.isNotEmpty()) {
                    CoroutineScope(IO).launch {
                        val upNextEpisode: Episode? = collectionDatabase.episodeDao().findByMediaId(upNextEpisodeMediaId)
                        withContext(Main) {
                            if (upNextEpisode != null) {
                                player.addMediaItem(1, CollectionHelper.buildMediaItem(this@BasePlayerService, upNextEpisode))
                            }
                        }
                    }
                }
            } else {
                // playback is not active - stop periodically saving the playback position to database
                handler.removeCallbacks(periodicProgressUpdateRunnable)

                // cancel sleep timer
                cancelSleepTimer(delayedReset = true)

                // Not playing because playback is paused, ended, suppressed, or the player
                // is buffering, stopped or failed. Check player.getPlayWhenReady,
                // player.getPlaybackState, player.getPlaybackSuppressionReason and
                // player.getPlaybackError for details.
                when (player.playbackState) {
                    // player is able to immediately play from its current position
                    Player.STATE_READY -> {
                        // todo
                    }
                    // buffering - data needs to be loaded
                    Player.STATE_BUFFERING -> {
                        // todo
                    }
                    // player finished playing all media
                    Player.STATE_ENDED -> {
                        player.stop()
                        player.removeMediaItem(0)
                    }
                    // initial state or player is stopped or playback failed
                    Player.STATE_IDLE -> {
                        // todo
                    }
                }
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Custom AnalyticsListener that enables AudioFX equalizer integration
     */
    private val analyticsListener = object: AnalyticsListener {
        override fun onAudioSessionIdChanged(eventTime: AnalyticsListener.EventTime, audioSessionId: Int) {
            super.onAudioSessionIdChanged(eventTime, audioSessionId)
            // integrate with system equalizer (AudioFX)
            val intent: Intent = Intent(AudioEffect.ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION)
            intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, audioSessionId)
            intent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, packageName)
            intent.putExtra(AudioEffect.EXTRA_CONTENT_TYPE, AudioEffect.CONTENT_TYPE_VOICE)
            sendBroadcast(intent)
            // note: remember to broadcast AudioEffect.ACTION_CLOSE_AUDIO_EFFECT_CONTROL_SESSION, when not needed anymore
        }
    }
    /*
     * End of declaration
     */


}