/*
 * ImageHelper.kt
 * Implements the ImageHelper object
 * An ImageHelper provides helper methods for image related operations
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.helpers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.net.toUri
import androidx.palette.graphics.Palette
import org.y20k.escapepod.R
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream


/*
 * ImageHelper class
 */
object ImageHelper {

    /* Define log tag */
    private val TAG: String = ImageHelper::class.java.simpleName

    /* Get scaling factor from display density */
    fun getDensityScalingFactor(context: Context): Float {
        return context.resources.displayMetrics.density
    }


    /* Get a podcast cover */
    fun getPodcastCover(context: Context, imageUriString: String, requestedSize: Int = 0, ignoreScreenDensity: Boolean = false): Bitmap {
        var bitmap: Bitmap? = null

        try {
            // CASE: create a bitmap that is scaled to the requested parameters
            if (requestedSize > 0 && imageUriString.isNotEmpty()) {
                // 1: calculate width
                val densityScalingFactor: Float
                if (ignoreScreenDensity) {
                    densityScalingFactor = 1f
                } else {
                    densityScalingFactor = ImageHelper.getDensityScalingFactor(context)
                }
                val size: Int = (requestedSize * densityScalingFactor).toInt()
                // 2: only retrieve the image dimensions
                var stream: InputStream? = context.contentResolver.openInputStream(imageUriString.toUri())
                val options: BitmapFactory.Options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeStream(stream, null, options)
                stream?.close()
                // 3: calculate the optimal inSampleSize and
                options.inJustDecodeBounds = false
                options.inSampleSize = calculateSampleParameter(options, size, size)
                // 4: decode the file
                stream = context.contentResolver.openInputStream(imageUriString.toUri())
                bitmap = BitmapFactory.decodeStream(stream, null, options)
                stream?.close()
            }
            // CASE: create a full size bitmap
            else {
                // just decode the file
                val stream: InputStream? = context.contentResolver.openInputStream(imageUriString.toUri())
                bitmap = BitmapFactory.decodeStream(stream)
                stream?.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // get default image, if the above did not work
        if (bitmap == null) {
            bitmap = ContextCompat.getDrawable(context, R.drawable.ic_default_cover_rss_icon_192dp)!!.toBitmap()
        }

        return bitmap
    }


    /* Get an unscaled version of the podcast cover as a ByteArray */
    fun getPodcastCoverAsByteArray(context: Context, imageUriString: String = String()): ByteArray {
        val coverBitmap: Bitmap = getPodcastCover(context, imageUriString, 0)
        val stream = ByteArrayOutputStream()
        coverBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val coverByteArray: ByteArray = stream.toByteArray()
        coverBitmap.recycle()
        return coverByteArray
    }


    /* Extracts color from an image */
    fun getMainColor(context: Context, imageUri: String): Int {

        // extract color palette from image
        val palette: Palette = Palette.from(getPodcastCover(context, imageUri, 72)).generate()
        // get muted and vibrant swatches
        val vibrantSwatch = palette.vibrantSwatch
        val mutedSwatch = palette.mutedSwatch

        when {
            vibrantSwatch != null -> {
                // return vibrant color
                val rgb = vibrantSwatch.rgb
                return Color.argb(255, Color.red(rgb), Color.green(rgb), Color.blue(rgb))
            }
            mutedSwatch != null -> {
                // return muted color
                val rgb = mutedSwatch.rgb
                return Color.argb(255, Color.red(rgb), Color.green(rgb), Color.blue(rgb))
            }
            else -> {
                // default return
                return context.resources.getColor(R.color.default_neutral_medium_light, null)
            }
        }
    }


    /* Calculates parameter needed to scale image down */
    private fun calculateSampleParameter(options: BitmapFactory.Options, requestedWidth: Int, requestedHeight: Int): Int {
        // get size of original image
        val originalHeight = options.outHeight
        val originalWidth = options.outWidth
        var inSampleSize = 1

        if (originalHeight > requestedHeight || originalWidth > requestedWidth) {

            val halfHeight = originalHeight / 2
            val halfWidth = originalWidth / 2

            // calculates the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width
            while (halfHeight / inSampleSize > requestedHeight && halfWidth / inSampleSize > requestedWidth) {
                inSampleSize *= 2
            }
        }
        return inSampleSize
    }


    /* Return sampled down image for given Uri */
    private fun decodeSampledBitmapFromUri_old(context: Context, imageUriString: String, reqWidth: Int, reqHeight: Int): Bitmap {

        var bitmap: Bitmap? = null
        if (imageUriString.isNotEmpty()) {
            try {
                val imageUri: Uri = imageUriString.toUri()

                // first decode with inJustDecodeBounds=true to check dimensions
                var stream: InputStream? = context.contentResolver.openInputStream(imageUri)
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeStream(stream, null, options)
                stream?.close()

                // calculate inSampleSize
                options.inSampleSize = calculateSampleParameter(options, reqWidth, reqHeight)

                // decode bitmap with inSampleSize set
                stream = context.contentResolver.openInputStream(imageUri)
                options.inJustDecodeBounds = false
                bitmap = BitmapFactory.decodeStream(stream, null, options)
                stream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        // get default image
        if (bitmap == null) {
            bitmap = ContextCompat.getDrawable(context, R.drawable.ic_default_cover_rss_icon_192dp)!!.toBitmap()
        }

        return bitmap
    }

}