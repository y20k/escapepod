/*
 * MainActivity.kt
 * Implements the MainActivity class
 * MainActivity just extends BaseMainActivity - the non-free version adds a Cast button listener here
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod


/*
 * MainActivity class
 */
class MainActivity: BaseMainActivity() {

}