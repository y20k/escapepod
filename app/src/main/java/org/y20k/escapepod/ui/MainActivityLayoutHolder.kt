/*
 * MainActivityLayoutHolder.kt
 * MainActivityLayoutHolder the LayoutHolder class
 * A MainActivityLayoutHolder holds references to the views of the MainActivity
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Vibrator
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.fragment.app.FragmentContainerView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.escapepod.Keys
import org.y20k.escapepod.R
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.objects.Episode
import org.y20k.escapepod.database.objects.EpisodeDescription
import org.y20k.escapepod.database.objects.Podcast
import org.y20k.escapepod.dialogs.ShowNotesDialog
import org.y20k.escapepod.helpers.DateTimeHelper
import org.y20k.escapepod.helpers.ImageHelper
import org.y20k.escapepod.helpers.PreferencesHelper


/*
 * MainActivityLayoutHolder class
 */
data class MainActivityLayoutHolder(val activity: AppCompatActivity, val rootView: View, val collectionDatabase: CollectionDatabase) {

    /* Define log tag */
    private val TAG: String = MainActivityLayoutHolder::class.java.simpleName


    /* Main class variables */
    private lateinit var systemBars: Insets
    private var mainHostContainer: FragmentContainerView
    private var downloadProgressIndicator: ProgressBar
    private var bottomSheet: ConstraintLayout
    private var playerViews: Group
    private var upNextViews: Group
    private var sheetPlayerViews: Group
    var sleepTimerRunningViews: Group
    private var coverView: ImageView
    private var podcastNameView: TextView
    private var episodeTitleView: TextView
    var playButtonView: ImageButton
    var bufferingIndicator: ProgressBar
    private var sheetCoverView: ImageView
    var sheetProgressBarView: Slider
    var sheetTimePlayedView: TextView
    var sheetDurationView: TextView
    private var sheetEpisodeTitleView: TextView
    var sheetPlayButtonView: ImageButton
    var sheetSkipBackButtonView: ImageButton
    var sheetSkipForwardButtonView: ImageButton
    var sheetSleepTimerStartButtonView: ImageButton
    var sheetSleepTimerCancelButtonView: ImageButton
    var sheetSleepTimerRemainingTimeView: TextView
    var sheetPlaybackSpeedButtonView: Button
    var sheetUpNextName: TextView
    var sheetUpNextClearButton: ImageButton
    //    private var onboardingLayout: ConstraintLayout
    private var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private var isBuffering: Boolean
    var displayTimeRemaining: Boolean

    /* Init block */
    init {
        // find views
        mainHostContainer = rootView.findViewById(R.id.main_host_container)
        downloadProgressIndicator = rootView.findViewById(R.id.download_progress_indicator)
        bottomSheet = rootView.findViewById(R.id.bottom_sheet)
        playerViews = rootView.findViewById(R.id.player_views)
        sheetPlayerViews = rootView.findViewById(R.id.sheet_player_views)
        sleepTimerRunningViews = rootView.findViewById(R.id.sleep_timer_running_views)
        upNextViews = rootView.findViewById(R.id.up_next_views)
        coverView = rootView.findViewById(R.id.player_podcast_cover)
        podcastNameView = rootView.findViewById(R.id.podcast_name)
        episodeTitleView = rootView.findViewById(R.id.player_episode_title)
        playButtonView = rootView.findViewById(R.id.player_play_button)
        bufferingIndicator = rootView.findViewById(R.id.player_buffering_indicator)
        sheetCoverView = rootView.findViewById(R.id.sheet_large_podcast_cover)
        sheetProgressBarView = rootView.findViewById(R.id.sheet_playback_slider)
        sheetTimePlayedView = rootView.findViewById(R.id.sheet_time_played_view)
        sheetDurationView = rootView.findViewById(R.id.sheet_duration_view)
        sheetEpisodeTitleView = rootView.findViewById(R.id.sheet_episode_title)
        sheetPlayButtonView = rootView.findViewById(R.id.sheet_play_button)
        sheetSkipBackButtonView = rootView.findViewById(R.id.sheet_skip_back_button)
        sheetSkipForwardButtonView = rootView.findViewById(R.id.sheet_skip_forward_button)
        sheetSleepTimerStartButtonView = rootView.findViewById(R.id.sleep_timer_start_button)
        sheetSleepTimerCancelButtonView = rootView.findViewById(R.id.sleep_timer_cancel_button)
        sheetSleepTimerRemainingTimeView = rootView.findViewById(R.id.sleep_timer_remaining_time)
        sheetPlaybackSpeedButtonView = rootView.findViewById(R.id.playback_speed_button)
        sheetUpNextName = rootView.findViewById(R.id.sheet_up_next_name)
        sheetUpNextClearButton = rootView.findViewById(R.id.sheet_up_next_clear_button)
//        onboardingLayout = rootView.findViewById(R.id.onboarding_layout)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        displayTimeRemaining = false
        isBuffering = false

        // make background of download progress bar semitransparent
        downloadProgressIndicator.background.alpha = 128

        // set up RecyclerView
        val animator: SimpleItemAnimator = DefaultItemAnimator()
        animator.supportsChangeAnimations = false

        // display minutes + seconds in progress bar thumb label
        sheetProgressBarView.setLabelFormatter(LabelFormatter { value ->
            DateTimeHelper.convertToMinutesAndSeconds(value.toLong())
        })

        // set up edge to edge display
        setupEdgeToEdge()

        // set layout for player
        setupBottomSheet()
    }


    /* Updates the player views */
    @SuppressLint("ClickableViewAccessibility")
    fun updatePlayerViews(context: Context, episode: Episode?) {
        if (episode != null) {
            val duration: String
            if (episode.duration > 0L) duration = DateTimeHelper.convertToMinutesAndSeconds(episode.duration) else duration = "∞"

            coverView.setImageBitmap(ImageHelper.getPodcastCover(context, episode.smallCover, 80))
            // coverView.clipToOutline = true // apply rounded corner mask to covers
            coverView.contentDescription = "${context.getString(R.string.descr_player_podcast_cover)}: ${episode.podcastName}"
            podcastNameView.text = episode.podcastName
            episodeTitleView.text = episode.title
            sheetCoverView.setImageBitmap(ImageHelper.getPodcastCover(context, episode.cover))
            // sheetCoverView.clipToOutline = true // apply rounded corner mask to covers
            sheetCoverView.contentDescription = "${context.getString(R.string.descr_expanded_player_podcast_cover)}: ${episode.podcastName}"
            sheetEpisodeTitleView.text = episode.title
            sheetDurationView.text = duration
            sheetDurationView.contentDescription = "${context.getString(R.string.descr_expanded_episode_length)}: $duration"
            sheetProgressBarView.valueTo = episode.duration.toFloat() + 1 // stupid hack to handle episodes with no known duration

            // setup progress bar
            updateProgressbar(context, episode.playbackPosition, episode.duration)

            // update click listeners
            sheetCoverView.setOnClickListener {
                displayShowNotes(context, episode)
            }
            sheetEpisodeTitleView.setOnClickListener {
                displayShowNotes(context, episode)
            }
            podcastNameView.setOnLongClickListener { view ->
                val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                v.vibrate(50)
                displayShowNotes(context, episode)
                return@setOnLongClickListener true
            }
            episodeTitleView.setOnLongClickListener {
                val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                v.vibrate(50)
                displayShowNotes(context, episode)
                return@setOnLongClickListener true
            }
        }
    }


    /* Toggles visibility of the download progress indicator */
    fun toggleDownloadProgressIndicator() {
        when (PreferencesHelper.loadActiveDownloads()) {
            Keys.ACTIVE_DOWNLOADS_EMPTY -> downloadProgressIndicator.isGone = true
            else -> downloadProgressIndicator.isVisible = true
        }
    }


    /* Loads and displays show notes */
    private fun displayShowNotes(context: Context, episode: Episode) {
        CoroutineScope(IO).launch {
            val podcast: Podcast? = collectionDatabase.podcastDao().findByRemotePodcastFeedLocation(episode.episodeRemotePodcastFeedLocation)
            val episodeDescription: EpisodeDescription? = collectionDatabase.episodeDescriptionDao().findByMediaId(episode.mediaId)
            if (episodeDescription != null && podcast != null) {
                withContext(Main) { ShowNotesDialog().show(context, podcast, episode, episodeDescription) }
            }
        }
    }


    /* Updates the progress bar */
    fun updateProgressbar(context: Context, position: Long, duration: Long) {
        val timePlayed = DateTimeHelper.convertToMinutesAndSeconds(position)
        sheetTimePlayedView.text = timePlayed
        sheetTimePlayedView.contentDescription = "${context.getString(R.string.descr_expanded_player_time_played)}: $timePlayed"
        if (position >= sheetProgressBarView.valueFrom && position <= sheetProgressBarView.valueTo) {
            sheetProgressBarView.value = position.toFloat()
        }
        if (displayTimeRemaining && duration > 0L) {
            val timeRemaining = DateTimeHelper.convertToMinutesAndSeconds((duration - position), negativeValue = true)
            sheetDurationView.text = timeRemaining
            sheetDurationView.contentDescription = "${context.getString(R.string.descr_expanded_player_time_remaining)}: $timeRemaining"
        } else if (duration > 0L || sheetDurationView.text == "∞") {
            sheetDurationView.text = DateTimeHelper.convertToMinutesAndSeconds(duration)
            sheetProgressBarView.valueTo = duration.toFloat() + 1 // stupid hack to handle episodes with no known duration
            sheetDurationView.contentDescription = context.getString(R.string.descr_expanded_episode_length)
        }
    }


    /* Updates the playback speed view */
    fun updatePlaybackSpeedView(context: Context, speed: Float = 1f) {
        val playbackSpeedButtonText: String = "$speed x"
        sheetPlaybackSpeedButtonView.text = playbackSpeedButtonText
        sheetPlaybackSpeedButtonView.contentDescription = "$playbackSpeedButtonText - ${context.getString(R.string.descr_expanded_player_playback_speed_button)}"
    }


    /* Updates the Up Next views */
    fun updateUpNextViews(upNextEpisode: Episode?) {
        when (upNextEpisode != null) {
            true -> {
                // show the Up Next queue if queue is not empty
                upNextViews.isGone = true // stupid hack - try to remove this line ASAP (https://stackoverflow.com/a/47893965)
                upNextViews.isVisible = true
                // update Up Next view
                val upNextName = "${upNextEpisode.podcastName} - ${upNextEpisode.title}"
                sheetUpNextName.text = upNextName
            }
            false -> {
                // hide the Up Next queue if queue is empty
                upNextViews.isGone = true // stupid hack - try to remove this line ASAP (https://stackoverflow.com/a/47893965)
                upNextViews.isVisible = false

            }
        }
    }


    /* Updates sleep timer views */
    fun updateSleepTimer(context: Context, timeRemaining: Long = 0L) {
        when (timeRemaining) {
            0L -> {
                if (!sleepTimerRunningViews.isGone) {
                    sleepTimerRunningViews.isGone = true
                    sheetSleepTimerRemainingTimeView.text = String()
                }
            }
            else -> {
                if (sheetPlayerViews.isVisible) {
                    sleepTimerRunningViews.isVisible = true
                    val sleepTimerTimeRemaining: String = DateTimeHelper.convertToMinutesAndSeconds(timeRemaining)
                    sheetSleepTimerRemainingTimeView.text = sleepTimerTimeRemaining
                    sheetSleepTimerRemainingTimeView.contentDescription = "${context.getString(R.string.descr_expanded_player_sleep_timer_remaining_time)}: $sleepTimerTimeRemaining"
                }
            }
        }
    }


    /* Toggles play/pause buttons */
    fun togglePlayButtons(isPlaying: Boolean) {
        when (isPlaying) {
            true -> {
                playButtonView.setImageResource(R.drawable.ic_player_pause_symbol_48dp)
                sheetPlayButtonView.setImageResource(R.drawable.ic_player_pause_symbol_68dp)
            }
            false -> {
                playButtonView.setImageResource(R.drawable.ic_player_play_symbol_48dp)
                sheetPlayButtonView.setImageResource(R.drawable.ic_player_play_symbol_68dp)
            }
        }
    }


    /* Toggles buffering indicator */
    fun showBufferingIndicator(buffering: Boolean) {
        bufferingIndicator.isVisible = buffering
        isBuffering = buffering
    }


//    /* Minimizes player sheet if expanded */
//    fun minimizePlayerIfExpanded(): Boolean {
//        return if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
//            true
//        } else {
//            false
//        }
//    }


//    /* Toggles visibility of player depending on playback state - hiding it when playback is stopped (not paused or playing) */
//    fun togglePlayerVisibility(playbackState: Int?): Boolean {
//        // possible states: STATE_IDLE, STATE_BUFFERING, STATE_READY, STATE_ENDED
//        when (playbackState) {
//            Player.STATE_IDLE, Player.STATE_ENDED, null -> return hidePlayer()
//            else -> return showPlayer()
//        }
//    }


//    /* Toggle the onboarding layout */
//    fun toggleOnboarding(context: Context, collectionSize: Int): Boolean {
//        if (collectionSize == 0) {
//            onboardingLayout.isVisible = true
//            hidePlayer(context)
//            return true
//        } else {
//            onboardingLayout.isGone = true
//            return false
//        }
//    }


    /* Initiates the rotation animation of the play button  */
    fun animatePlaybackButtonStateTransition(context: Context, isPlaying: Boolean) {
        var morphDrawable: AnimatedVectorDrawable? = null
        if (isPlaying) {
            // set up play/pause button animation
            when (bottomSheetBehavior.state) {
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    playButtonView.setImageResource(R.drawable.anim_play_to_pause_48dp)
                    morphDrawable = playButtonView.drawable as AnimatedVectorDrawable
                }

                BottomSheetBehavior.STATE_EXPANDED -> {
                    sheetPlayButtonView.setImageResource(R.drawable.anim_play_to_pause_68dp)
                    morphDrawable = sheetPlayButtonView.drawable as AnimatedVectorDrawable
                }
            }
        } else {
            when (bottomSheetBehavior.state) {
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    playButtonView.setImageResource(R.drawable.anim_pause_to_play_48dp)
                    morphDrawable = playButtonView.drawable as AnimatedVectorDrawable
                }
                BottomSheetBehavior.STATE_EXPANDED -> {
                    sheetPlayButtonView.setImageResource(R.drawable.anim_pause_to_play_68dp)
                    morphDrawable = sheetPlayButtonView.drawable as AnimatedVectorDrawable
                }
            }
        }

        if (morphDrawable != null) {
            // rotate and morph to play/pause icon
            morphDrawable.start()
            morphDrawable.registerAnimationCallback(object : Animatable2.AnimationCallback() {
                override fun onAnimationEnd(drawable: Drawable?) {
                    togglePlayButtons(isPlaying)
                }
            })
        } else {
            // just toggle the play/pause icon
            togglePlayButtons(isPlaying)
        }
    }


    /* Shows player */
    fun showPlayer(): Boolean {
        addBottomPadding()
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_HIDDEN || bottomSheetBehavior.state == BottomSheetBehavior.STATE_SETTLING) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            bottomSheet.postDelayed({bottomSheetBehavior.isHideable = false}, 50) // disable hiding again - give the bottom sheet time to settle
        }
        return true
    }


    /* Hides player */
    fun hidePlayer(): Boolean {
        removeBottomPadding()
        bottomSheetBehavior.isHideable = true // temporarily allow hiding
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
//        bottomSheet.postDelayed({bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN}, 50)
        return true
    }


    /* Shows the player views and hides the large player sheet views */
    private fun showPlayerViews() {
        playerViews.isVisible = true
        sheetPlayerViews.isGone = true
        if (isBuffering) {
            bufferingIndicator.isVisible = true
        }
    }


    /* Shows the large player sheet views and hides the player views */
    private fun showPlayerSheetViews() {
        playerViews.isGone = true
        sheetPlayerViews.isVisible = true
        bufferingIndicator.isVisible = false
        if (sheetSleepTimerRemainingTimeView.text.isEmpty()) {
            sleepTimerRunningViews.isGone = true
        }
    }


    /* Toggle expanded/collapsed state of bottom sheet */
    private fun toggleBottomSheetState() {
        when (bottomSheetBehavior.state) {
            BottomSheetBehavior.STATE_COLLAPSED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            else -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }


    /* Sets up the player (BottomSheet) */
    private fun setupBottomSheet() {
        // show / hide the small player
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(view: View, slideOffset: Float) {
                if (slideOffset < 0.25f) {
                    showPlayerViews()
                } else {
                    showPlayerSheetViews()
                }
            }
            override fun onStateChanged(view: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_COLLAPSED -> showPlayerViews()
                    BottomSheetBehavior.STATE_DRAGGING -> Unit // do nothing
                    BottomSheetBehavior.STATE_EXPANDED -> showPlayerSheetViews()
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> Unit // do nothing
                    BottomSheetBehavior.STATE_SETTLING -> Unit // do nothing
                    BottomSheetBehavior.STATE_HIDDEN -> showPlayerViews()
                }
            }
        })
        // toggle collapsed state on tap
        bottomSheet.setOnClickListener { toggleBottomSheetState() }
        coverView.setOnClickListener { toggleBottomSheetState() }
        podcastNameView.setOnClickListener { toggleBottomSheetState() }
        episodeTitleView.setOnClickListener { toggleBottomSheetState() }
    }


    /* Sets up margins/paddings for edge to edge view - for API 35 and above */
    private fun setupEdgeToEdge() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.VANILLA_ICE_CREAM) {
            ViewCompat.setOnApplyWindowInsetsListener(rootView) { v, insets ->
                // get measurements for status and navigation bar
                systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars() or WindowInsetsCompat.Type.displayCutout())
                // apply measurements to the bottom sheet
                bottomSheetBehavior.peekHeight = systemBars.bottom + (Keys.BOTTOM_SHEET_PEEK_HEIGHT * ImageHelper.getDensityScalingFactor(rootView.context)).toInt()
                downloadProgressIndicator.updateLayoutParams<ConstraintLayout.LayoutParams> {
                    topMargin = systemBars.top
                }
                bottomSheet.updateLayoutParams<CoordinatorLayout.LayoutParams> {
                    bottomMargin = systemBars.top // bottomMargin is actually the top margin of the bottom sheet ¯\_(ツ)_/¯
                }
                bottomSheet.updatePadding(0, 0, 0, systemBars.bottom)
                // apply measurements to the fragment container containing the podcast list
                mainHostContainer.updatePadding(
                    left = systemBars.left,
                    top = systemBars.top,
                    right = systemBars.right,
                    // bottom padding is updated in showPlayer() and in hidePlayer()
                )
                // return the insets
                insets // todo: or should I use WindowInsetsCompat.CONSUMED ?
            }
        } else {
            // deactivate edge to edge
            rootView.fitsSystemWindows = true
        }
    }


    /* Adds bottom padding for the fragment container containing podcast list, episode list, settings (use case: player is visible) */
    private fun addBottomPadding() {
        if (rootView.isLaidOut) {
            // determine needed bottom padding
            val bottomPadding: Int
            if (this::systemBars.isInitialized) {
                bottomPadding = systemBars.bottom + (Keys.BOTTOM_SHEET_PEEK_HEIGHT * ImageHelper.getDensityScalingFactor(rootView.context)).toInt()
            } else {
                bottomPadding = (Keys.BOTTOM_SHEET_PEEK_HEIGHT * ImageHelper.getDensityScalingFactor(rootView.context)).toInt()
            }
            // apply bottom padding
            mainHostContainer.updatePadding(bottom = bottomPadding)
        } else {
            // wait until the view is laid out
            rootView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    rootView.viewTreeObserver.removeOnGlobalLayoutListener(this) // avoid multiple calls
                    // determine needed bottom padding
                    val bottomPadding: Int
                    if (this@MainActivityLayoutHolder::systemBars.isInitialized) {
                        bottomPadding = systemBars.bottom + (Keys.BOTTOM_SHEET_PEEK_HEIGHT * ImageHelper.getDensityScalingFactor(rootView.context)).toInt()
                    } else {
                        bottomPadding = (Keys.BOTTOM_SHEET_PEEK_HEIGHT * ImageHelper.getDensityScalingFactor(rootView.context)).toInt()
                    }
                    // apply bottom padding
                    mainHostContainer.updatePadding(bottom = bottomPadding)
                }
            })
        }
    }


    /* Removes bottom padding for the fragment container containing podcast list, episode list, settings (use case: player is hidden) */
    private fun removeBottomPadding() {
        if (rootView.isLaidOut) {
            // determine needed bottom padding
            val bottomPadding: Int
            if (this::systemBars.isInitialized) {
                bottomPadding = systemBars.bottom // todo bottom should be 0?
            } else {
                bottomPadding = 0
            }
            // apply bottom padding
            mainHostContainer.updatePadding(bottom = bottomPadding)
        } else {
            // wait until the view is laid out
            rootView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    rootView.viewTreeObserver.removeOnGlobalLayoutListener(this) // avoid multiple calls
                    // determine needed bottom padding
                    val bottomPadding: Int
                    if (this@MainActivityLayoutHolder::systemBars.isInitialized) {
                        bottomPadding = systemBars.bottom // todo bottom should be 0?
                    } else {
                        bottomPadding = 0
                    }
                    // apply bottom padding
                    mainHostContainer.updatePadding(bottom = bottomPadding)
                }
            })
        }
    }

}