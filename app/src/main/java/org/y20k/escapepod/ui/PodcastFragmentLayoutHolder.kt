/*
 * PodcastFragmentLayoutHolder.kt
 * Implements the PodcastFragmentLayoutHolder class
 * A PodcastFragmentLayoutHolder hold references to the views of the PodcastFragment
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Vibrator
import android.view.View
import android.view.ViewTreeObserver.OnPreDrawListener
import android.widget.Filterable
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.core.net.toUri
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.escapepod.Keys
import org.y20k.escapepod.R
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.objects.Podcast
import org.y20k.escapepod.database.objects.PodcastDescription
import org.y20k.escapepod.dialogs.DescriptionDialog
import org.y20k.escapepod.extensions.setCleanHtml
import org.y20k.escapepod.helpers.DownloadHelper
import org.y20k.escapepod.helpers.ImageHelper
import org.y20k.escapepod.helpers.UiHelper


/*
 * LayoutHolder class
 */
data class PodcastFragmentLayoutHolder(val activity: Activity, val rootView: View, val collectionDatabase: CollectionDatabase, val filtered: Boolean) {

    /* Define log tag */
    private val TAG: String = PodcastFragmentLayoutHolder::class.java.simpleName


    /* Main class variables */
    val swipeRefreshLayout: SwipeRefreshLayout
    val episodesList: RecyclerView
    val topbarPodcastNameView: MaterialTextView
    val filterIcon: ImageView
    val filterText: MaterialTextView
    val coverView: ImageView
    lateinit var podcast: Podcast
    lateinit var podcastDescription: PodcastDescription
    private val layoutManager: LinearLayoutManager
    private val scroller: RecyclerView.SmoothScroller
    private val topbarBackButton: ImageButton
    private val podcastDescriptionView: MaterialTextView
    private val podcastDescriptionFadeView: View
    private val podcastWebsiteView: MaterialTextView
    private val podcastLinksDivider: ImageView
    private val podcastFeedView: MaterialTextView
    private var podcastCoverUriString: String = String()


    /* Init block */
    init {
        // find views
        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_podcast_layout)
        topbarBackButton = rootView.findViewById(R.id.arrow_back)
        topbarPodcastNameView = rootView.findViewById(R.id.topbar_title)
        episodesList = rootView.findViewById(R.id.episodes_list)
        filterIcon = rootView.findViewById(R.id.filter_icon)
        filterText = rootView.findViewById(R.id.filter_text)
        coverView = rootView.findViewById(R.id.podcast_cover)
//        podcastNameView = rootView.findViewById(R.id.podcast_name)
        podcastDescriptionView = rootView.findViewById(R.id.podcast_description)
        podcastDescriptionFadeView = rootView.findViewById(R.id.podcast_description_fade)
        podcastWebsiteView = rootView.findViewById(R.id.podcast_website)
        podcastLinksDivider = rootView.findViewById(R.id.divider_centered_dot)
        podcastFeedView = rootView.findViewById(R.id.podcast_feed)

        // set up back button
        val navController: NavController = Navigation.findNavController(activity, R.id.main_host_container)
        topbarBackButton.setOnClickListener {
            navController.navigateUp()
        }

        // set up filter
        if (filtered) {
            filterText.text = activity.getText(R.string.episode_list_filter_downloaded)
        } else {
            filterText.text = activity.getText(R.string.podcast_list_button_all_episodes)
        }

        // set up the description fade
        podcastDescriptionFadeView.background = UiHelper.createColorTransparencyGradientDrawable(activity, R.color.list_podcast_episodes_background, 0.1f)

        // set up RecyclerView
        layoutManager = CustomLayoutManager(rootView.context)
        episodesList.layoutManager = layoutManager
        val animator: SimpleItemAnimator = DefaultItemAnimator()
        animator.supportsChangeAnimations = true
        episodesList.itemAnimator = animator

        // customize scroller
        scroller = object : LinearSmoothScroller(rootView.context) {
            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_START
            }
        }
    }


    /* Scrolls recycler list to top */
    fun scrollToTop() {
        scroller.targetPosition = 0
        layoutManager.startSmoothScroll(scroller)
    }


    /* Updates the podcast views */
    @SuppressLint("ClickableViewAccessibility")
    fun updatePodcastViews() {
        // podcast name and cover
        setPodcastCover(podcast.smallCover)
        setPodcastName(podcast.name)
        // coverView.clipToOutline = true // apply rounded corner mask to covers
        coverView.contentDescription = "${activity.getString(R.string.descr_player_podcast_cover)}: ${podcast.name}"
        coverView.setOnLongClickListener {
            DownloadHelper.refreshCover(activity, podcast)
            val v = activity.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(50)
            // v.vibrate(VibrationEffect.createOneShot(50, android.os.VibrationEffect.DEFAULT_AMPLITUDE)); // todo check if there is an androidx vibrator
            return@setOnLongClickListener true
        }
        // podcast website: set up open browser
        if (podcast.website.isNotEmpty()) {
            podcastWebsiteView.isVisible = true
            podcastLinksDivider.isVisible = true
            podcastWebsiteView
            podcastWebsiteView.setOnClickListener {
                startActivity(activity, Intent(Intent.ACTION_VIEW, podcast.website.toUri()), null)
            }
        } else {
            podcastWebsiteView.isGone = true
            podcastLinksDivider.isGone = true
        }
        // podcast feed: set up clipboard copy
        podcastFeedView.setOnClickListener {
            val clip: ClipData = ClipData.newPlainText("simple text", podcast.remotePodcastFeedLocation)
            val cm: ClipboardManager = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cm.setPrimaryClip(clip)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU){
                Toast.makeText(activity, R.string.toast_message_copied_to_clipboard, Toast.LENGTH_LONG).show()
            }
        }
        // update the filter label
        updateFilterLabel(podcast.episodeListDisplayFilter)
    }


    /* Toggles the downloaded episodes filter */
    fun toggleFilter() {
        if (this::podcast.isInitialized) {
            when (podcast.episodeListDisplayFilter) {
                Keys.FILTER_SHOW_DOWNLOADED -> {
                    CoroutineScope(IO).launch {
                        collectionDatabase.podcastDao().updateEpisodeListFilter(podcast.remotePodcastFeedLocation, Keys.FILTER_SHOW_ALL)
                        withContext(Main) {
                            updateFilterLabel(Keys.FILTER_SHOW_ALL)
                            filterEpisodeList(Keys.FILTER_SHOW_ALL)
                        }
                    }
                }
                else -> {
                    CoroutineScope(IO).launch {
                        collectionDatabase.podcastDao().updateEpisodeListFilter(podcast.remotePodcastFeedLocation, Keys.FILTER_SHOW_DOWNLOADED)
                        withContext(Main) {
                            updateFilterLabel(Keys.FILTER_SHOW_DOWNLOADED)
                            filterEpisodeList(Keys.FILTER_SHOW_DOWNLOADED)
                        }
                    }
                }
            }
        }
    }


    /* Update the downloaded episodes filter label */
    private fun updateFilterLabel(filter: Int) {
        when (filter) {
            Keys.FILTER_SHOW_DOWNLOADED -> {
                filterText.text = activity.getString(R.string.episode_list_filter_downloaded)
            }

            else -> {
                filterText.text = activity.getString(R.string.episode_list_filter_all)
            }
        }
    }


    /* Filter the episode list */
    private fun filterEpisodeList(filter: Int) {
        val adapter: Filterable = episodesList.adapter as Filterable
        adapter.filter.filter(filter.toString())
    }


    /* Updates the podcast description view */
    fun updatePodcastDescriptionView() {
        // podcast description: fit into available space in layout / expands on tap
        // Credit: https://stackoverflow.com/a/46904071/14326132
        if (this::podcastDescription.isInitialized) {
//            podcastDescriptionView.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
//                override fun onGlobalLayout() {
//                    podcastDescriptionView.viewTreeObserver.removeOnGlobalLayoutListener(this)
//                    val numberOfLinesVisible: Int = podcastDescriptionView.height / podcastDescriptionView.lineHeight
//                    podcastDescriptionView.setCleanHtml(podcastDescription.description)
//                    val podcastDescriptionLineCount: Int = podcastDescriptionView.lineCount
//                    podcastDescriptionView.maxLines = numberOfLinesVisible
//                    podcastDescriptionView.ellipsize = TextUtils.TruncateAt.END
//                    // check if text fade needed
//                    podcastDescriptionTooLong = podcastDescriptionLineCount > podcastDescriptionView.maxLines
//                    togglePodcastDescriptionFadeView()
//                }
//            })
            // set the description
            podcastDescriptionView.setCleanHtml(podcastDescription.description)
            // check if text fade needed
            podcastDescriptionView.viewTreeObserver.addOnPreDrawListener (object : OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    podcastDescriptionView.viewTreeObserver.removeOnPreDrawListener(this)
                    val numberOfLinesVisible: Int = podcastDescriptionView.height / podcastDescriptionView.lineHeight
                    podcastDescriptionView.setCleanHtml(podcastDescription.description)
                    val podcastDescriptionLineCount: Int = podcastDescriptionView.lineCount
                    podcastDescriptionView.maxLines = numberOfLinesVisible
                    togglePodcastDescriptionFadeView(podcastDescriptionLineCount > podcastDescriptionView.maxLines)
                    return true
                }
            })
            podcastDescriptionView.setOnClickListener {
                DescriptionDialog().show(activity, podcast, podcastDescription.description)
            }
        }
    }


    /* Toggles the text fade at the end of the description */
    private fun togglePodcastDescriptionFadeView(podcastDescriptionTooLong: Boolean) {
        if (podcastDescriptionTooLong) {
            podcastDescriptionFadeView.visibility = View.VISIBLE
        } else {
            podcastDescriptionFadeView.visibility = View.GONE
        }
    }


    /* Sets the podcast cover */
    fun setPodcastCover(newPodcastCoverUriString: String) {
        // only set, if it has not yet been set
        if (!this::podcast.isInitialized || podcastCoverUriString != newPodcastCoverUriString) {
            coverView.setImageBitmap(ImageHelper.getPodcastCover(activity, newPodcastCoverUriString, Keys.SIZE_COVER_PODCAST_CARD))
            podcastCoverUriString = newPodcastCoverUriString
        }
    }


    /* Sets the podcast name */
    fun setPodcastName(podcastCoverString: String) {
        // only set, if it has not yet been set
        if (!this::podcast.isInitialized) {
            topbarPodcastNameView.text = podcastCoverString
        } else if (topbarPodcastNameView.text != podcastCoverString || topbarPodcastNameView.text != podcastCoverString) {
            topbarPodcastNameView.text = podcastCoverString
        }
    }


    /*
     * Inner class: Custom LinearLayoutManager
     */
    private inner class CustomLayoutManager(context: Context): LinearLayoutManager(context, VERTICAL, false) {
        override fun supportsPredictiveItemAnimations(): Boolean {
            return true
        }
    }
    /*
     * End of inner class
     */
}