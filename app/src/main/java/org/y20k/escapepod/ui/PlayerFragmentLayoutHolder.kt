/*
 * PlayerFragmentLayoutHolder.kt
 * Implements the PlayerFragmentLayoutHolder class
 * A PlayerFragmentLayoutHolder holds references to the views of the PlayerFragment
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.ui

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import org.y20k.escapepod.Keys
import org.y20k.escapepod.R
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.helpers.ImageHelper


/*
 * PlayerFragmentLayoutHolder class
 */
data class PlayerFragmentLayoutHolder(val rootView: View, val collectionDatabase: CollectionDatabase) {

    /* Define log tag */
    private val TAG: String = PlayerFragmentLayoutHolder::class.java.simpleName


    /* Main class variables */
    val swipeRefreshLayout: SwipeRefreshLayout
    val podcastList: RecyclerView
    val onboardingLayout: ConstraintLayout
    val layoutManager: LinearLayoutManager
    private val scroller: RecyclerView.SmoothScroller

    /* Init block */
    init {
        // find views
        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_collection_layout)
        podcastList = rootView.findViewById(R.id.podcast_list)
        onboardingLayout = rootView.findViewById(R.id.onboarding_layout)

        // set up RecyclerView
        layoutManager = CustomLayoutManager(rootView.context)
        podcastList.layoutManager = layoutManager
        val animator: SimpleItemAnimator = DefaultItemAnimator()
        animator.supportsChangeAnimations = true
        podcastList.itemAnimator = animator
        podcastList.setHasFixedSize(true)
        podcastList.setItemViewCacheSize(10)

        // customize scroller
        scroller = object : LinearSmoothScroller(rootView.context) {
            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_START
            }
        }
    }


    /* Scrolls recycler list to top */
    fun scrollToTop() {
        scroller.targetPosition = 0
        layoutManager.startSmoothScroll(scroller)
    }


    /* Toggle the onboarding layout */
    fun toggleOnboarding(collectionSize: Int): Boolean {
        if (collectionSize == 0) {
            onboardingLayout.isVisible = true
            return true
        } else {
            onboardingLayout.isGone = true
            return false
        }
    }


    /*
     * Inner class: Custom ItemDecoration (usage: podcastList.addItemDecoration(CustomItemDecoration(bars.top, bars.bottom)))
     */
    private inner class CustomItemDecoration(private val topMargin: Int, private val bottomMargin: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view)
            // apply margins only to the first and the last item
            if (position == 0) {
                outRect.top = topMargin
            } else if (position == state.itemCount - 1) {
                outRect.bottom = bottomMargin + (Keys.BOTTOM_SHEET_PEEK_HEIGHT * ImageHelper.getDensityScalingFactor(parent.context)).toInt()
            }
        }
    }
    /*
     * End of inner class
     */



    /*
     * Inner class: Custom LinearLayoutManager
     */
    private inner class CustomLayoutManager(context: Context): LinearLayoutManager(context, VERTICAL, false) {
        override fun supportsPredictiveItemAnimations(): Boolean {
            return true
        }
    }
    /*
     * End of inner class
     */
}