/*
 * EpisodeViewHolder.kt
 * Implements the EpisodeViewHolder class
 * A EpisodeViewHolder is a custom view holder for an episode for use in a recyclerview
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.ui

import android.content.Context
import android.os.Vibrator
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.escapepod.R
import org.y20k.escapepod.collection.EpisodeListener
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.objects.Episode
import org.y20k.escapepod.database.objects.EpisodeDescription
import org.y20k.escapepod.database.objects.Podcast
import org.y20k.escapepod.dialogs.ShowNotesDialog
import org.y20k.escapepod.helpers.DateTimeHelper


/*
 * EpisodeViewHolder class
 */
class EpisodeViewHolder (layout: View): RecyclerView.ViewHolder(layout) {

    /* Main class variables */
    val episodeDateView: TextView = layout.findViewById(R.id.episode_date)
    val episodeTitleView: TextView = layout.findViewById(R.id.episode_title)
    val episodeDownloadButtonView: ImageButton = layout.findViewById(R.id.episode_download_button)
    val episodeDeleteButtonView: ImageButton = layout.findViewById(R.id.episode_delete_button)
    val episodePlayButtonView: ImageButton = layout.findViewById(R.id.episode_play_button)
    val episodePlaybackProgressView: ProgressBar = layout.findViewById(R.id.episode_playback_progress)
    val episodePlaybackViews: Group = layout.findViewById(R.id.episode_playback_views)
    var tapAnywherePlaybackEnabled: Boolean = false
    lateinit var episode: Episode


    /* Initializes the view holder */
    fun initialize(context: Context, collectionDatabase: CollectionDatabase, episodeListener: EpisodeListener) {
        // show the episode description on long click
        episodeTitleView.setOnLongClickListener {
            showEpisodeDescription(context, collectionDatabase)
            return@setOnLongClickListener true
        }
        episodeDateView.setOnLongClickListener {
            showEpisodeDescription(context, collectionDatabase)
            return@setOnLongClickListener true
        }
        // play/pause on click
        episodeDateView.setOnClickListener {
            if (tapAnywherePlaybackEnabled && episodePlayButtonView.isVisible) {
                episodePlayButtonView.performClick()
            }
        }
        episodeTitleView.setOnClickListener {
            if (tapAnywherePlaybackEnabled && episodePlayButtonView.isVisible) {
                episodePlayButtonView.performClick()
            }
        }
        episodePlayButtonView.setOnClickListener {
            episodeListener.onPlayButtonTapped(episode)
        }
        // mark episode listened on long click
        episodePlayButtonView.setOnLongClickListener {
            val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(50)
            // v.vibrate(VibrationEffect.createOneShot(50, android.os.VibrationEffect.DEFAULT_AMPLITUDE)); // todo check if there is an androidx vibrator
            episodeListener.onMarkListenedButtonTapped(episode)
            return@setOnLongClickListener true
        }
        // download episode on click
        episodeDownloadButtonView.setOnClickListener {
            episodeListener.onDownloadButtonTapped(episode)
        }
        // download all episodes on long click
        episodeDownloadButtonView.setOnLongClickListener {
            val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(50)
            // v.vibrate(VibrationEffect.createOneShot(50, android.os.VibrationEffect.DEFAULT_AMPLITUDE)); // todo check if there is an androidx vibrator
            episodeListener.onDownloadAllButtonTapped(episode.episodeRemotePodcastFeedLocation)
            return@setOnLongClickListener true
        }
        // delete episode on click
        episodeDeleteButtonView.setOnClickListener {
            episodeListener.onDeleteButtonTapped(episode)
        }
    }


    /* Sets up the circular progress bar */
    fun setEpisodePlaybackProgress() {
        // start => 12 => playbackPosition = 0
        // finish => 0 => playbackPosition = duration
        val progress: Double = (episode.duration.toDouble() - episode.playbackPosition.toDouble()) / episode.duration.toDouble() * 12
        episodePlaybackProgressView.progress = progress.toInt()
    }


    /* Sets up an episode's title views */
    fun setEpisodeTitle() {
        episodeDateView.text = DateTimeHelper.getDateString(episode.publicationDate)
        episodeTitleView.text = episode.title
    }


    /* Sets up an episode's play, download and delete button views (for CollectionAdapter) */
    fun setEpisodeButtons() {
        when (episode.isPlaying) {
            true -> episodePlayButtonView.setImageResource(R.drawable.ic_pause_symbol_24dp)
            false -> episodePlayButtonView.setImageResource(R.drawable.ic_play_symbol_24dp)
        }
        if (episode.audio.isNotEmpty()) {
            episodeDownloadButtonView.isGone = true
            episodePlaybackViews.isVisible = true
        } else {
            episodeDownloadButtonView.isVisible = true
            episodePlaybackViews.isGone = true
        }
    }


    /* Show the episode description */
    private fun showEpisodeDescription(context: Context, collectionDatabase: CollectionDatabase) {
        CoroutineScope(Dispatchers.IO).launch {
            val episodeDescription: EpisodeDescription? = collectionDatabase.episodeDescriptionDao().findByMediaId(episode.mediaId)
            val podcast: Podcast? = collectionDatabase.podcastDao().findByRemotePodcastFeedLocation(episode.episodeRemotePodcastFeedLocation)
            if (episodeDescription != null && podcast != null) {
                withContext(Dispatchers.Main) { ShowNotesDialog().show(context, podcast, episode, episodeDescription) }
            }
        }
        val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        v.vibrate(50)
        // v.vibrate(VibrationEffect.createOneShot(50, android.os.VibrationEffect.DEFAULT_AMPLITUDE)); // todo check if there is an androidx vibrator
    }



}
