/*
 * PodcastViewHolder.kt
 * Implements the PodcastViewHolder class
 * A PodcastViewHolder is a custom view holder for a podcast for use in a recyclerview
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.escapepod.ui

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import org.y20k.escapepod.Keys
import org.y20k.escapepod.R
import org.y20k.escapepod.database.wrappers.PodcastWithLatestEpisodeWrapper
import org.y20k.escapepod.helpers.DownloadHelper


/*
 * PodcastViewHolder class
 */
class PodcastViewHolder (podcastCardLayout: View): RecyclerView.ViewHolder(podcastCardLayout) {

    /* Main class variables */
    val podcastImageView: ImageView = podcastCardLayout.findViewById(R.id.podcast_cover)
    val podcastNameView: TextView = podcastCardLayout.findViewById(R.id.podcast_name)
    val currentEpisodeView: EpisodeViewHolder = EpisodeViewHolder(podcastCardLayout)
    val allEpisodesButtonView: MaterialButton = podcastCardLayout.findViewById(R.id.older_episodes_toggle)
    private lateinit var navigateToPodcastFragmentsArgs: Bundle
    private lateinit var podcast: PodcastWithLatestEpisodeWrapper

    /* Initializes the view holder */
    fun initialize(context: Context) {
        // refresh podcast cover on long click
        podcastImageView.setOnLongClickListener {
            DownloadHelper.refreshCover(context, podcast.data)
            val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(50)
            // v.vibrate(VibrationEffect.createOneShot(50, android.os.VibrationEffect.DEFAULT_AMPLITUDE)); // todo check if there is an androidx vibrator
            return@setOnLongClickListener true
        }
        // go to all episodes list on click
        allEpisodesButtonView.text = context.getString(R.string.podcast_list_button_all_episodes)
        val extras = FragmentNavigatorExtras(podcastImageView to "podcast_cover_transition", podcastNameView to "podcast_name_transition")
        allEpisodesButtonView.setOnClickListener {
            it.findNavController().navigate(R.id.podcast_destination, navigateToPodcastFragmentsArgs, null, extras)
        }
        podcastImageView.setOnClickListener {
            it.findNavController().navigate(R.id.podcast_destination, navigateToPodcastFragmentsArgs, null, extras)
        }
    }


    /* Sets the podcast (lateinit) for this view holder */
    fun setPodcast(newPodcast: PodcastWithLatestEpisodeWrapper) {
        podcast = newPodcast
        navigateToPodcastFragmentsArgs = bundleOf(
            Keys.ARG_PODCAST_FEED to newPodcast.data.remotePodcastFeedLocation,
            Keys.ARG_PODCAST_NAME to newPodcast.data.name,
            Keys.ARG_PODCAST_COVER to newPodcast.data.smallCover,
            Keys.ARG_EPISODE_LIST_FILTER to newPodcast.data.episodeListDisplayFilter
        )
        // todo test if podcast has any episodes
    }


    /* Sets the podcast name view */
    fun setPodcastName() {
        podcastNameView.text = podcast.data.name
    }

}
